# Distributed Flexibility FLA for Parameterization of Algorithms in MAS

This repository contains the code implementation of the research paper titled 
"Distributed Flexibility Fitness Landscape Analysis for Parameterization of Algorithms in Multi-Agent Energy Systems".

This paper addresses the challenge of assessing the aggregated flexibility of a set of energy resources. 
To this end, a novel approach that utilizes fitness landscape analysis to explore the characteristics of power system 
optimization problems based on asset flexibility was proposed.  
The practical application of the approach was presented with a use case involving the distributed optimization of energy 
resource scheduling using the COHDA.

## Contents
The repository contains the following files and directories:

- `src/`: This directory includes the source code implementation of the proposed flexibility landscape analysis and the COHDA-based energy resource scheduling optimization with the agent-framework mango.
  - `src/meta_optimization/`: This directory contains ML-models implemented in order to learn optimal reaction time parameters in COHDA based on information from the flexibility fitness landscape analysis 
- `data/`: This directory contains all required data resources and generated scenario data used for the application use case.
- `README.md`: This file provides an overview of the repository and instructions for usage.
- `pysimmods/`: This directory contains a fork of the pip-package pysimmods.

![Activity diagram](images/Activity%20diagram.png)
The illustration above shows the sequence of the scenario that is started by executing the [start script](start.py).

![Sequence diagram](images/Sequence%20diagram.png)
The sequence diagram shows the detailed procedure.

![Class diagram](images/Class%20diagram.png)
All classes used in the implementation are displayed in the class diagram. 

## Requirements
The following dependencies are required to run the code successfully:

- Python (version 3.8)
- Libraries: can be found in the `requirements.txt`

## Installation
1. Clone the repository to your local machine using the following command:
   ```
   git clone https://gitlab.com/malin_radtke/distributed-flexibility-fla-for-parameterization-of-algorithms-in-mas.git
   ```
2. Change the working directory to the repository:
   ```
   cd flex-flas
   ```
3. Install the required Python libraries using pip:
   ```
   pip install -r requirements.txt
   ```

## Usage
All relevant parameters can be set in the [start script](start.py). 
To start the scenario, just execute the script.

## Citation
If you use this code or research findings for your work, please cite the original paper:

[Include the citation information of the research paper]
