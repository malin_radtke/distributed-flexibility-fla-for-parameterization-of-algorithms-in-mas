import asyncio
from copy import deepcopy
from datetime import datetime
import os
from typing import List, Tuple, Union, Dict
import matplotlib.pyplot as plt
import pandas as pd
import networkx as nx
import numpy as np

from mango.core.container import Container
from mango.messages.codecs import JSON
from mango.role.core import RoleAgent
from mango_library.coalition.core import CoalitionParticipantRole, CoalitionInitiatorRole, CoalitionModel
from mango_library.negotiation.cohda.cohda_negotiation import COHDANegotiationRole, CohdaSolutionModel
from mango_library.negotiation.cohda.cohda_solution_aggregation import CohdaSolutionAggregationRole
from mango_library.negotiation.cohda.cohda_starting import CohdaNegotiationStarterRole
from mango_library.negotiation.termination import NegotiationTerminationDetectorRole, \
    NegotiationTerminationParticipantRole
import mango_library.negotiation.util as mango_util

import src.definitions as definitions
import src.util as util
from src.agents import FlexAgent, ControllerAgent
from src.messages import FlexRequest, FlexRequestAck, FlexibilitySample, FeatureRequest, FeatureInformation, \
    FeatureInformationAck
from src.observed_container import ObservedContainer
from pysimmods.other.flexibility.flexibilities import Flexibilities


async def main(num_agents, neighborhood_definition,
               walk_definition, model_configurations, check_inbox_intervals, topology_edge_probabilities,
               send_messages_delayed):
    await execute_flexibility_fitness_landscape_calculation(num_agents, neighborhood_definition,
                                                            walk_definition, model_configurations)
    await asyncio.sleep(1)
    for run in range(5):
        for check_inbox_interval in check_inbox_intervals:
            for topology_p in topology_edge_probabilities:
                await execute_cohda_negotiation(num_agents, check_inbox_interval,
                                                topology_p, send_messages_delayed)
                await asyncio.sleep(5)


async def execute_cohda_negotiation(num_agents, check_inbox_interval, topology_p, send_messages_delayed):
    codec = JSON()
    codec2 = JSON()
    for serializer in mango_util.cohda_serializers:
        codec.add_serializer(*serializer())
        codec2.add_serializer(*serializer())

    port = 5555

    controller_container = await ObservedContainer.factory(addr=("localhost", port), codec=codec,
                                                           send_messages_delayed=send_messages_delayed)
    port += 1

    flex_container = await ObservedContainer.factory(addr=("localhost", port), codec=codec2,
                                                     send_messages_delayed=send_messages_delayed)

    G = nx.newman_watts_strogatz_graph(n=num_agents, k=2, p=topology_p)

    def small_world_creator(participants: List[Tuple[str, Union[str, Tuple[str, int]], str]]) -> \
            Dict[Tuple[str, Union[str, Tuple[str, int]], str],
                 List[Tuple[str, Union[str, Tuple[str, int]], str]]]:
        """Create a small world topology

        :param participants: the list of all participants

        :return: a map, mapping every participant to a list of their neighbors
        """
        part_to_neighbors = {}
        for part_i, part in enumerate(participants):
            part_to_neighbors[part] = [participants[x] for x in list(nx.all_neighbors(G, part_i))]
        return part_to_neighbors

    controller_agent = RoleAgent(controller_container)

    controller_agent.add_role(NegotiationTerminationDetectorRole(aggregator_addr=controller_container.addr,
                                                                 aggregator_id=controller_agent.aid))
    aggregation_role = CohdaSolutionAggregationRole()
    controller_agent.add_role(aggregation_role)

    addrs = []

    # read flexibilities from file
    all_flexibilities = [Flexibilities().from_json(open(f'data/flexibility_schedules/Flex Agent {i}.json').read())
                         for i in range(num_agents)]
    # get schedules out of flexibility objects
    all_schedules = [flexibility._schedules for flexibility in all_flexibilities]
    # get values from schedules
    all_schedules = [list(schedule.values()) for schedule in all_schedules]
    # get data out of Schedule objects
    all_schedules_data = list()
    for schedules in all_schedules:
        schedules_data = [list(schedule._data['p_kw'].values) for schedule in schedules]
        all_schedules_data.append(schedules_data)

    # run scenario
    print(100 * '-')
    print('Scenario ID: ', definitions.Constants.SCENARIO_UUID)
    print('Run scenario with configuration: \n',
          'Check inbox interval: ', check_inbox_interval[0], '\n',
          'Target schedule:', definitions.Constants.TARGET_SCHEDULE, '\n',
          'Topology p parameter: ', topology_p)
    print(100 * '-')
    start_time = datetime.now()

    flex_agents = list()
    for flex_agent_i in range(num_agents):
        # agents always live inside a container
        flex_agent = RoleAgent(flex_container, suggested_aid=f'Flex Agent {flex_agent_i}')

        def schedules_provider(idx):
            return deepcopy(lambda: all_schedules_data[idx])

        cohda_role = COHDANegotiationRole(schedules_provider=schedules_provider(flex_agent_i),
                                          local_acceptable_func=lambda s: True,
                                          check_inbox_interval=check_inbox_interval[1][flex_agent_i])
        flex_agent.add_role(cohda_role)
        flex_agent.add_role(CoalitionParticipantRole())
        flex_agent.add_role(NegotiationTerminationParticipantRole())
        addrs.append((flex_container.addr, flex_agent._aid))
        flex_agents.append(flex_agent)

    controller_agent.add_role(CoalitionInitiatorRole(addrs, 'cohda', 'cohda-negotiation',
                                                     topology_creator=small_world_creator))
    await asyncio.wait_for(wait_for_coalition_built(flex_agents + [controller_agent]), timeout=5)

    await asyncio.wait_for(wait_for_assignment(flex_agents), timeout=10)

    flex_agents[0].add_role(CohdaNegotiationStarterRole((definitions.Constants.TARGET_SCHEDULE,
                                                         [1] * len(definitions.Constants.TARGET_SCHEDULE))))

    try:
        await asyncio.wait_for(wait_for_term_detection(controller_agent), timeout=2000)
        for flex_agent in flex_agents:
            await asyncio.wait_for(wait_for_solution_received(flex_agent), timeout=10)
        # write results
        perf = list(aggregation_role.cohda_solutions.values())[0].perf
        duration = (datetime.now() - start_time).seconds
        messages_sent = flex_container.messages_sent + controller_container.messages_sent
        cluster_schedule = list(aggregation_role.cohda_solutions.values())[0].cluster_schedule
        results = {
            'scenario': definitions.Constants.SCENARIO_UUID,
            'check_inbox_interval': check_inbox_interval[0],
            'topology p': topology_p,
            'performance': perf,
            'duration': duration,
            'messages sent': messages_sent
        }
        print('Results: \nPerformance: ', perf, ',  duration: ', duration, ', messages sent: ', messages_sent,
              '\nCluster schedules: ')
        for schedule_i, schedule in enumerate(cluster_schedule):
            print('Sum of schedule ', schedule_i, ': ', sum(schedule))
        # plot_schedules(definitions.Constants.TARGET_SCHEDULE, cluster_schedule, topology_p, check_inbox_interval)

        filename = f'scenario_data/negotiation_{str(definitions.Constants.SCENARIO_UUID)}.csv'
        if os.path.exists(filename):
            print(f'The file {filename} exists.')
            cohda_results_df = pd.read_csv(filename)
            cohda_results_df = cohda_results_df.append(results, ignore_index=True)

        else:
            cohda_results_df = pd.DataFrame(results, index=[0])
            print(f'The file {filename} does not exist.')

        for column in cohda_results_df.columns:
            if 'Unnamed' in column:
                cohda_results_df.drop(columns=column, inplace=True)
        cohda_results_df.to_csv(f'scenario_data/negotiation_{definitions.Constants.SCENARIO_UUID}.csv')
    except asyncio.exceptions.TimeoutError:
        print('Timeout error')

    await asyncio.sleep(5)

    for flex_agent in flex_agents:
        await flex_agent.shutdown()

    await controller_agent.shutdown()

    # always properly shut down your containers
    await flex_container.shutdown()
    await controller_container.shutdown()


def plot_schedules(target_schedule, cluster_schedule, topology_p, check_inbox_interval):
    plt.plot(target_schedule, label="Target schedule")
    plt.plot(sum(cluster_schedule), label="Cluster schedule")
    for schedule in cluster_schedule:
        plt.plot(schedule, linestyle='dashed', color='moccasin')
    extension = f'_top_{topology_p}_check_inbox_{check_inbox_interval}'
    extension = extension.replace('.', ',')
    plt.ylabel('Fitness value')
    plt.xlabel('Time intervals')
    plt.legend(loc="upper left")

    util.directory_exists_or_create(dir_name='target_and_cluster_schedules')
    plt.savefig('target_and_cluster_schedules/target_schedule_' + str(definitions.Constants.SCENARIO_UUID) + extension)
    plt.clf()


async def execute_flexibility_fitness_landscape_calculation(num_agents, neighborhood_definition,
                                                            walk_definition, model_configurations):
    codec = JSON()
    codec.add_serializer(*FlexRequest.__serializer__())
    codec.add_serializer(*FlexRequestAck.__serializer__())
    codec.add_serializer(*FlexibilitySample.__serializer__())
    codec.add_serializer(*FeatureRequest.__serializer__())
    codec.add_serializer(*FeatureInformation.__serializer__())
    codec.add_serializer(*FeatureInformationAck.__serializer__())
    for serializer in mango_util.cohda_serializers:
        codec.add_serializer(*serializer())

    port = 5555

    controller_container = await Container.factory(addr=("localhost", port), codec=codec)
    port += 1

    flex_container = await Container.factory(addr=("localhost", port), codec=codec)

    flex_agents = list()
    for flex_agent_i in range(num_agents):
        # agents always live inside a container
        flex_agents.append(FlexAgent(flex_container,
                                     suggested_aid=f'Flex Agent {flex_agent_i}',
                                     neighborhood_definition=neighborhood_definition,
                                     walk_definition=walk_definition,
                                     model_configuration=model_configurations[flex_agent_i]))

    controller_agent = ControllerAgent(controller_container, known_agents=flex_agents, suggested_aid='Controller')

    await asyncio.sleep(30)
    for flex_agent in flex_agents:
        await flex_agent.roles[0]._received_feature_information_ack

    for flex_agent in flex_agents:
        await flex_agent.shutdown()

    await controller_agent.shutdown()

    # always properly shut down your containers
    await flex_container.shutdown()
    await controller_container.shutdown()


async def wait_for_solution_confirmed(aggregation_role):
    while len(aggregation_role.cohda_solutions) == 0:
        await asyncio.sleep(1)


async def wait_for_coalition_built(agents):
    for agent in agents:
        while not agent.inbox.empty():
            await asyncio.sleep(0.1)
    print('coalition build')


async def wait_for_term_detection(controller_agent):
    # Function that will return once the first weight map of the given agent equals to one
    while (len(controller_agent.roles[0]._weight_map.values()) != 1 or
           list(controller_agent.roles[0]._weight_map.values())[0] != 1):
        await asyncio.sleep(0.05)


async def wait_for_assignment(cohda_agents):
    for agent in cohda_agents:
        while len(agent.roles[0].context.get_or_create_model(CoalitionModel).assignments) == 0:
            await asyncio.sleep(0.1)
    print('assignments received')


async def wait_for_solution_received(cohda_agent):
    while len(cohda_agent._agent_context.get_or_create_model(CohdaSolutionModel)._final_schedules) == 0:
        await asyncio.sleep(0.05)
