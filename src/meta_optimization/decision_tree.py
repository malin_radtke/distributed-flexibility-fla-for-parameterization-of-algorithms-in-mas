import os

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'

from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.tree import DecisionTreeClassifier

from src.meta_optimization.optimization_util import load_training_and_test_data

x_train, y_train, dependent_variables = load_training_and_test_data(get_dependent_variables=True,
                                                                    path_to_data='/scenario_data_single_change_label/',
                                                                    get_scenario_uuid=True)
total_data = x_train
total_data['fitness'] = y_train
total_data = total_data[(total_data.r == 1)].append(total_data[(total_data.r == 5)])

# Filter for the rows with the lowest fitness value for each scenario
lowest_fitness_rows = total_data.groupby("scenario_uuid")["fitness"].idxmin()

# Create a new DataFrame with the filtered rows
filtered_df = total_data.loc[lowest_fitness_rows].reset_index()

filtered_df.drop(columns=['fitness', 'scenario_uuid', 'index'], inplace=True)

# Split the data into features (X) and labels (y)
X = filtered_df.drop(columns=['r'])
y = filtered_df['r']

# Create an instance of the DecisionTreeClassifier
decision_tree = DecisionTreeClassifier()

# Fit the decision tree model to the data
decision_tree.fit(X, y)

# Visualize the decision tree (optional)
# You can use graphviz or other tools to generate a visual representation of the decision tree

# Make predictions using the decision tree
predictions = decision_tree.predict(X)

# Evaluate the accuracy of the decision tree
accuracy = decision_tree.score(X, y)
print("Accuracy:", accuracy)

# Plot the feature importances (optional)
feature_importances = decision_tree.feature_importances_
plt.figure(figsize=(10, 6))
sns.barplot(x=feature_importances, y=filtered_df.columns[:-1])
plt.xlabel("Feature Importance")
plt.ylabel("Features")
plt.title("Decision Tree Feature Importances")
plt.show()