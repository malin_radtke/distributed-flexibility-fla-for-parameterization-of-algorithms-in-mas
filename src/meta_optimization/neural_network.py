import os

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'
from keras.optimizers import Adam
from keras.utils import to_categorical
from matplotlib import pyplot as plt
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
from tensorflow import keras
import seaborn as sns

from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

x_train, y_train, dependent_variables = load_training_and_test_data(get_dependent_variables=True,
                                                                    path_to_data='/scenario_data_single_change_label/',
                                                                    get_scenario_uuid=True)
total_data = x_train
total_data['fitness'] = y_train
#total_data = total_data[(total_data.r == 1)].append(total_data[(total_data.r == 3)]).append(
#    total_data[(total_data.r == 2)])

# Filter for the rows with the lowest fitness value for each scenario
lowest_fitness_rows = total_data.groupby("scenario_uuid")["fitness"].idxmin()

# Create a new DataFrame with the filtered rows
filtered_df = total_data.loc[lowest_fitness_rows].reset_index()

filtered_df.drop(columns=['fitness', 'scenario_uuid', 'index'], inplace=True)

# Separate input features (X) and target variable (y)
X = filtered_df.drop('r', axis=1)
y = filtered_df['r']

# Calculate the correlation matrix
corr_matrix = X.corrwith(y, method="spearman")

# Set a correlation threshold (e.g., 0.5)
correlation_threshold = 0.1

# Filter the input features based on the correlation threshold
selected_features = X.columns[abs(corr_matrix) >= correlation_threshold]

# Create a new DataFrame with the selected features
X = X[selected_features]

print('Selected features: ', list(selected_features))

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=3, shuffle=True)

# Convert the target variable to one-hot encoded vectors
label_encoder = LabelEncoder()
y_train = label_encoder.fit_transform(y_train)
y_test = label_encoder.fit_transform(y_test)
y_train = to_categorical(y_train, num_classes=5)
y_test = to_categorical(y_test, num_classes=5)

# Create a neural network model
print('input shape: ', X_train.shape[1], )
model = keras.Sequential([
    keras.layers.Dense(32, activation='relu', input_shape=(X_train.shape[1],)),
    keras.layers.Dense(16, activation='relu'),
    keras.layers.Dense(5, activation='softmax')
])

# Compile the model
model.compile(optimizer=Adam(learning_rate=0.001), loss='categorical_crossentropy', metrics=['accuracy'])

print(list(check_inbox_labels.keys())[0], len(filtered_df[(filtered_df.r == 1)]))
print(list(check_inbox_labels.keys())[1], len(filtered_df[(filtered_df.r == 2)]))
print(list(check_inbox_labels.keys())[2], len(filtered_df[(filtered_df.r == 3)]))
print(list(check_inbox_labels.keys())[3], len(filtered_df[(filtered_df.r == 4)]))
print(list(check_inbox_labels.keys())[4], len(filtered_df[(filtered_df.r == 5)]))

# Train the model
history = model.fit(X_train, y_train, epochs=50, batch_size=16, verbose=1)

# Evaluate the model on the test set
loss, accuracy = model.evaluate(X_test, y_test)
print("Test Loss:", loss)
print("Test Accuracy:", accuracy)

# Make predictions
y_pred_prob = model.predict(X_test)

# Convert predicted probabilities to class labels
y_pred_labels = np.argmax(y_pred_prob, axis=1)

# Convert one-hot encoded target variable back to class labels
y_test_labels = np.argmax(y_test, axis=1)

# Calculate the confusion matrix
cm = confusion_matrix(y_test_labels, y_pred_labels)

# Plot the confusion matrix using a heatmap
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", cbar=False, xticklabels=check_inbox_labels.keys(),
            yticklabels=check_inbox_labels.keys())

# Highlight the correct predictions
for i in range(len(cm)):
    plt.text(i + 0.5, i + 0.5, cm[i, i], ha='center', va='center', color='white', fontsize=12, fontweight='bold')

plt.title("Confusion Matrix")
plt.xlabel("Predicted")
plt.ylabel("Actual")
plt.show()
