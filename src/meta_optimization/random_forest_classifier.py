import os

from sklearn import tree

from src.util import directory_exists_or_create

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import seaborn as sns

from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

x_train, y_train, dependent_variables = load_training_and_test_data(
    path_to_data='/scenario_data_single_change_label/',
    get_dependent_variables=True,
    get_scenario_uuid=True)
total_data = x_train
total_data['fitness'] = y_train

#total_data = total_data[(total_data.r == 1)].append(total_data[(total_data.r == 2)])\
#    .append(total_data[(total_data.r == 4)]).append(total_data[(total_data.r == 5)])

lowest_fitness_rows = total_data.groupby("scenario_uuid")["fitness"].idxmin()
filtered_df = total_data.loc[lowest_fitness_rows].reset_index()

filtered_df.drop(columns=['fitness', 'scenario_uuid', 'index'], inplace=True)

X = filtered_df.drop('r', axis=1)
y = filtered_df['r']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Define the parameter grid for grid search
param_grid = {
    'n_estimators': [100, 500, 1000],
    'max_depth': [5, 10, 15]
}

# Create a Random Forest classifier
rf_classifier = RandomForestClassifier(random_state=42)

# Perform grid search to find the best hyperparameters
grid_search = GridSearchCV(estimator=rf_classifier, param_grid=param_grid, scoring='accuracy')
grid_search.fit(X_train, y_train)

# Get the best classifier and its hyperparameters
best_classifier = grid_search.best_estimator_
best_params = grid_search.best_params_

print("Best Hyperparameters:", best_params)

# Visualize the decision tree of the best classifier
_ = tree.plot_tree(best_classifier.estimators_[0], feature_names=X.columns, fontsize=6, filled=False, max_depth=3)

directory_exists_or_create('tree_structures')
plt.savefig(f'tree_structures/random_forest_classifier')
plt.clf()

# Predict on the test set using the best classifier
y_pred = best_classifier.predict(X_test)

# Evaluate the accuracy of the best classifier
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)

# Create a confusion matrix
cm = confusion_matrix(y_test, y_pred)

# Plot the confusion matrix using a heatmap
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Purples", cbar=False, xticklabels=check_inbox_labels.keys(),
            yticklabels=check_inbox_labels.keys())

plt.title("Classification for best class of parameter r")
plt.xlabel("Predicted")
plt.ylabel("Actual")
plt.savefig(f'classification_results/random_forest_classifier')
plt.clf()

