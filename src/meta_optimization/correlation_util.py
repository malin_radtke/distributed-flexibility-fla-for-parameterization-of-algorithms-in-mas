from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform
import pandas as pd
import seaborn as sns
import numpy as np


def save_correlation_analysis_as_file(df, method="spearman", fig_name=""):
    dissimilarity = 1 - abs(df.corr())
    Z = linkage(squareform(dissimilarity), 'complete')
    dendrogram(Z, labels=df.columns, orientation='top',
               leaf_rotation=90)
    # Clusterize the data
    threshold = 0.8
    labels = fcluster(Z, threshold, criterion='distance')
    # Keep the indices to sort labels
    labels_order = np.argsort(labels)
    # Build a new dataframe with the sorted columns
    for idx, i in enumerate(df.columns[labels_order]):
        if idx == 0:
            clustered = pd.DataFrame(df[i])
        else:
            df_to_append = pd.DataFrame(df[i])
            clustered = pd.concat([clustered, df_to_append], axis=1)
    plt.figure(figsize=(15, 10))
    correlations = clustered.corr(method=method)
    sns.set(font_scale=1.5)

    sns.heatmap(round(correlations, 2))
    sns.clustermap(correlations, method="complete", cmap='RdBu', annot=False,
                   annot_kws={"size": 7}, vmin=-1, vmax=1, figsize=(15, 12))
    plt.yticks(rotation=0)
    plt.xticks(rotation=70)

    plt.savefig('correlation_results/' + fig_name)
    plt.clf()
