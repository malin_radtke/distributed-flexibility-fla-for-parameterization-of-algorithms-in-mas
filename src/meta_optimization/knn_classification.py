import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

x_train, y_train, dependent_variables = load_training_and_test_data(get_dependent_variables=True,
                                                                    path_to_data='/scenario_data_single_change_label/',
                                                                    get_scenario_uuid=True)
total_data = x_train
total_data['fitness'] = y_train
# Filter for the rows with the lowest fitness value for each scenario
lowest_fitness_rows = total_data.groupby("scenario_uuid")["fitness"].idxmin()

# Create a new DataFrame with the filtered rows
filtered_df = total_data.loc[lowest_fitness_rows].reset_index()

filtered_df.drop(columns=['fitness', 'scenario_uuid', 'index'], inplace=True)

# Separate input features (X) and target variable (y)
X = filtered_df.drop('r', axis=1)  # Replace 'y' with the actual column name for the target variable
y = filtered_df['r']  # Replace 'y' with the actual column name for the target variable

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Train a K-Nearest Neighbors classifier
knn_classifier = KNeighborsClassifier(n_neighbors=3)
knn_classifier.fit(X_train, y_train)

# Predict on the test set
y_pred = knn_classifier.predict(X_test)

# Evaluate the accuracy of the classifier
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)

# Create a confusion matrix
cm = confusion_matrix(y_test, y_pred)

# Plot the confusion matrix using a heatmap
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", cbar=False, xticklabels=check_inbox_labels.keys(),
            yticklabels=check_inbox_labels.keys())

# Highlight the correct predictions
for i in range(len(cm)):
    plt.text(i + 0.5, i + 0.5, cm[i, i], ha='center', va='center', color='white', fontsize=12, fontweight='bold')

plt.title("Confusion Matrix")
plt.xlabel("Predicted")
plt.ylabel("Actual")
plt.show()
