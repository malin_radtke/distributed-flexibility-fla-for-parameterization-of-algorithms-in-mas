import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as px

from src import util
from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

total_data, y_train, dependent_variables = load_training_and_test_data(
    path_to_data='/scenario_data_single_change_label/',
    get_dependent_variables=True,
    get_scenario_uuid=False)

metrics = list(total_data.columns)
metrics.remove('r')

fitness_variables = list(dependent_variables.columns)

for dep_var in fitness_variables:
    total_data[dep_var] = dependent_variables[dep_var]

total_data['fitness'] = y_train

util.directory_exists_or_create('correlation_results/relation_metrics_and_r_to_fitness')
plt.rcParams["mathtext.fontset"] = "cm"

line_styles = {str(label): style for label, style in zip(check_inbox_labels.values(), [':', ':', ':', ':', ':'])}
lss = [':', '-', '--', '-.', ':']
custom_gray_palette = ['#EE6677', '#DDAA33', '#BB5566', '#004488', '#000000']


for legend in [True, False]:
    for col in metrics:
        for idx, dep_col in enumerate(['fitness']):
            sns.set(rc={'figure.figsize': (16, 9), 'figure.dpi': 300})
            sns.set_style('dark')
            sns.set(style='white', rc={'axes.facecolor': (0, 0, 0, 0)}, font_scale=1)
            sns.set_style("whitegrid", {'axes.grid': False})
            fig = px.scatter(total_data, x=col, y=dep_col, facet_col="r", color='r', trendline="ols")

            g = sns.lmplot(x=col, y=dep_col, data=total_data, hue='r',
                           legend=[], palette=custom_gray_palette,
                           scatter_kws={'alpha': 0.1}, scatter=False)
            g.set_ylabels(r'$f_{cohda}$')
            g.fig.subplots_adjust(top=0.9, right=0.65, wspace=0.2, hspace=0.2, bottom=0.086, left=0.07)
            if legend:
                line_legend = plt.legend(title='r', loc='upper right')
                # Update the legend labels with the string values
                for t, l in zip(line_legend.texts, check_inbox_labels.keys()):
                    l = l.replace('_', ' ')
                    t.set_text(l)
            g.set_titles(f'Relation between {col} and {dep_col} per r')

            g.set_axis_labels('', r'$performance$', fontsize=15)

            col = col.replace('*', '')

            g.savefig(
                'correlation_results/relation_metrics_and_r_to_fitness/' + dep_col + '_' + col + '_' + str(legend),
                bbox_inches='tight')
            plt.clf()
            plt.close()
