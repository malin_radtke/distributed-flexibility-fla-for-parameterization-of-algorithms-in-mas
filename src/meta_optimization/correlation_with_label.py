import os

from src.meta_optimization.correlation_util import save_correlation_analysis_as_file

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'

from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

x_train, y_train, dependent_variables = load_training_and_test_data(
    path_to_data='/scenario_data_single_change_label/',
    get_dependent_variables=True,
    get_scenario_uuid=True)
total_data = x_train
total_data['fitness'] = y_train

total_data = total_data[(total_data.r == 1)].append(total_data[(total_data.r == 3)]).append(
    total_data[(total_data.r == 2)])

# Filter for the rows with the lowest fitness value for each scenario
lowest_fitness_rows = total_data.groupby("scenario_uuid")["fitness"].idxmin()

# Create a new DataFrame with the filtered rows
filtered_df = total_data.loc[lowest_fitness_rows].reset_index()

print(list(check_inbox_labels.keys())[0], len(filtered_df[(filtered_df.r == 1)]))
print(list(check_inbox_labels.keys())[1], len(filtered_df[(filtered_df.r == 2)]))
print(list(check_inbox_labels.keys())[2], len(filtered_df[(filtered_df.r == 3)]))
print(list(check_inbox_labels.keys())[3], len(filtered_df[(filtered_df.r == 4)]))
print(list(check_inbox_labels.keys())[4], len(filtered_df[(filtered_df.r == 5)]))

filtered_df.drop(columns=['fitness', 'scenario_uuid', 'index'], inplace=True)

save_correlation_analysis_as_file(filtered_df, fig_name='correlation_with_class_r')
print('Correlation analysis saved ad file "correlation_with_class_r"')
