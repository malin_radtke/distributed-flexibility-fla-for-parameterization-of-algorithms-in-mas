import matplotlib.pyplot as plt
import seaborn as sns

from src.meta_optimization.optimization_util import load_training_and_test_data, check_inbox_labels

total, fitness, dependent_variables = load_training_and_test_data(
    path_to_data='/scenario_data_single_change_label/',
    get_dependent_variables=True,
    get_scenario_uuid=True)

for var in dependent_variables.columns:
    total[var] = dependent_variables[var]

plt.rcParams["mathtext.fontset"] = "cm"

total[r'$f_{cohda}$'] = fitness

for key, value in check_inbox_labels.items():
    total['r'].replace(value, key.replace('_', ' '), inplace=True)

sns.set()

total.rename(columns={
    'duration': r'$duration$',
    'performance': r'$solution$ $quality$',
    'messages sent': r'$messages$ $sent$'
}, inplace=True)

sns.set(font_scale=1.5)

# Create a box plot using matplotlib and pandas
axes = total.boxplot(column=[r'$duration$', r'$solution$ $quality$', r'$messages$ $sent$', r'$f_{cohda}$'], by='r',
                     figsize=(15, 15))

axes[1, 0].tick_params(axis='x', which='major', labelsize=18, rotation=90)

axes[1, 1].tick_params(axis='x', which='major', labelsize=18, rotation=90)


# Display the plot
plt.show()
