import os

from src.meta_optimization.correlation_util import save_correlation_analysis_as_file

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'

from src.meta_optimization.optimization_util import load_training_and_test_data

df, _, dependent_variables = load_training_and_test_data(get_dependent_variables=True,
                                                               get_scenario_uuid=True)
# Find duplicate rows based on 'Column1'
df = df.drop_duplicates(subset='scenario_uuid', keep="first")

df.drop(columns=['r', 'scenario_uuid'], inplace=True)

save_correlation_analysis_as_file(df, fig_name='correlation_between_metrics')
