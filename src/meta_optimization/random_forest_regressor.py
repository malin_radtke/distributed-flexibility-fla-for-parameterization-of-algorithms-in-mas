import os

from src.util import directory_exists_or_create

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn import tree

from src.meta_optimization.optimization_util import load_training_and_test_data

X, fitness, dependent_variables = load_training_and_test_data(get_dependent_variables=True,
                                                              path_to_data='/scenario_data_single_change_label/',
                                                              get_scenario_uuid=False)

X['fitness'] = fitness
#X = X[(X.r == 1)].append(X[(X.r == 2)]).append(X[(X.r == 3)]).append(X[(X.r == 4)]).reset_index()
fitness = X['fitness']
X.drop(columns=['fitness'], inplace=True)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, fitness, test_size=0.3, random_state=42)

# Define the parameter grid for grid search
param_grid = {
    'n_estimators': [100, 500, 1000],
    'max_depth': [5, 10, 15]
}

# Create a Random Forest regressor
rf_regressor = RandomForestRegressor(random_state=42)

# Perform grid search to find the best hyperparameters
grid_search = GridSearchCV(estimator=rf_regressor, param_grid=param_grid, scoring='neg_mean_squared_error')
grid_search.fit(X_train, y_train)

# Get the best model and its hyperparameters
best_regressor = grid_search.best_estimator_
best_params = grid_search.best_params_

print("Best Hyperparameters:", best_params)

# Predict on the test set using the best model
y_pred = best_regressor.predict(X_test)

# Evaluate the mean squared error of the best model
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)

_ = tree.plot_tree(best_regressor.estimators_[0], feature_names=X.columns, fontsize=10, filled=True, max_depth=3)
plt.title("Decision Tree Plot", fontsize=20)

directory_exists_or_create('tree_structures')
plt.savefig(f'tree_structures/decision_tree')
plt.clf()

# Plot the predicted vs actual values
for r in [1, 2, 3, 4]:
    r_y_test = [(y_test.values[idx], y_pred[idx]) for idx, i in enumerate(X_test['r'].values) if i == r]
    plt.scatter([x[0] for x in r_y_test], [x[1] for x in r_y_test], label=f'r={r}')
plt.legend()
plt.xlim(0, 3)
plt.ylim(0, 3)
plt.xlabel("Actual")
plt.ylabel("Predicted")
plt.title("Regression for fitness value")
directory_exists_or_create('regression_results/')
plt.savefig('regression_results/random_forest_regressor')
