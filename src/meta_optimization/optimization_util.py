import numpy as np
import pandas as pd
import glob
import os
import warnings
import matplotlib.pyplot as plt
from numpy import VisibleDeprecationWarning
from pandas.errors import SettingWithCopyWarning
from sklearn.inspection import permutation_importance
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns

import src.util as util
from start import ROOT_DIR

DATA_DIR = ROOT_DIR + "/data/"

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=VisibleDeprecationWarning)
warnings.filterwarnings("ignore", category=SettingWithCopyWarning)

columns = {
    'INFORMATION_CONTENT_MEAN': 'mean(H(e))',
    'INFORMATION_CONTENT_STD': 'std(H(e))',
    'PARTIAL_INFORMATION_CONTENT_MEAN': 'mean(M(e))',
    'PARTIAL_INFORMATION_CONTENT_STD': 'std(M(e))',
    'INFORMATION_STABILITY_MEAN': 'mean(e*)',
    'INFORMATION_STABILITY_STD': 'std(e*)',
    'FITNESS_VARIANCE_MEAN_MEAN': 'mean(mean(var))',
    'FITNESS_VARIANCE_MEAN_STD': 'std(mean(var))',
    'FITNESS_VARIANCE_STD_MEAN': 'mean(std(var))',
    'FITNESS_VARIANCE_STD_STD': 'std(std(var))',
    'FITNESS_MEAN_MEAN': 'mean(mean(f))',
    'FITNESS_MEAN_STD': 'std(mean(f))',
    'DISPERSION_INDEX_MEAN': 'mean(DI)',
    'DISPERSION_INDEX_STD': 'std(DI)',
    'MEAN_NUMBER_OF_NEUTRAL_AREAS_MEAN': 'mean(mean(num_na))',
    'MEAN_NUMBER_OF_NEUTRAL_AREAS_STD': 'std(mean(num_na))',
    'MEAN_SIZE_OF_NEUTRAL_AREAS_MEAN': 'mean(mean(size_na))',
    'MEAN_SIZE_OF_NEUTRAL_AREAS_STD': 'std(mean(size_na))',
    'MEAN_FITNESS_DISTANCE_MEAN': 'mean(mean(f_dist))',
    'MEAN_FITNESS_DISTANCE_STD': 'std(mean(f_dist))',
    'MAX_FITNESS_DISTANCE_MEAN': 'mean(max(f_dist))',
    'MAX_FITNESS_DISTANCE_STD': 'std(max(f_dist))',
    'check_inbox_interval': 'r',
}

check_inbox_labels = {
    'small_equal': 1,
    'medium_equal': 2,
    'high_equal': 3,
    'half_small_half_high': 4,
    'random': 5
}

VARIABLES = list(columns.keys())

DEPENDENT_VARIABLES = ['performance', 'duration', 'messages sent']
WEIGHTS = [1, 1, 1]
# default: aggregated_fitness -> contains all dependent variables weighted by given weights
PREDICTED_PARAMETER = 'aggregated_fitness'


def get_data(path):
    df = pd.DataFrame()
    for filename in glob.glob(os.path.join(path, '*.csv')):
        if 'negotiation' in filename:
            results_df = pd.read_csv(filename)
            scaler = MinMaxScaler()
            arr_scaled = scaler.fit_transform(results_df[DEPENDENT_VARIABLES])
            normalized_results = pd.DataFrame(arr_scaled, columns=DEPENDENT_VARIABLES)
            results_df.drop(columns=DEPENDENT_VARIABLES, inplace=True)
            normalized_results['aggregated_fitness'] = 0
            for i, variable in enumerate(DEPENDENT_VARIABLES):
                normalized_results['aggregated_fitness'] += normalized_results[variable] * WEIGHTS[i]
            results_df = results_df.join(normalized_results)
            scenario_filename = filename.replace('negotiation_', '')
            if os.path.exists(scenario_filename):
                scenario_df = pd.read_csv(scenario_filename)
            else:
                raise RuntimeError('Scenario data does not exist. ')
            for index, row in results_df.iterrows():
                new_part = scenario_df.copy()
                for column in results_df.columns:
                    if type(row[column]) == float:
                        new_part[column] = abs(row[column])
                    else:
                        new_part[column] = row[column]
                df = df.append(new_part, ignore_index=True)
    for column in df.columns:
        if 'Unnamed: 0' in column or 'Agent' in column or 'FLA_ID' in column:
            df.drop(columns=column, inplace=True)
    return df


def load_training_and_test_data(path_to_data=None, get_dependent_variables=False, get_scenario_uuid=False):
    """
    Loads training data from previous executed scenarios.
    :return: X,y which are dependent and independent variables.
    """
    # load data
    if path_to_data:
        path_training = DATA_DIR + path_to_data
    else:
        path_training = DATA_DIR + '/train_and_test_data/'
    df = get_data(path_training)
    dependent_variables = None

    scaler = MinMaxScaler()

    if not df.empty:
        df['check_inbox_interval'] = df['check_inbox_interval'].map(check_inbox_labels)
        if not get_dependent_variables:
            x_train_y_train = df[VARIABLES + [PREDICTED_PARAMETER]]
        else:
            x_train_y_train = df[VARIABLES + [PREDICTED_PARAMETER] + DEPENDENT_VARIABLES]
        x_train_y_train.dropna(inplace=True)
        x_train = x_train_y_train[VARIABLES]
        r = x_train['check_inbox_interval']
        arr_scaled = scaler.fit_transform(x_train)
        x_train = pd.DataFrame(arr_scaled, columns=x_train.columns, index=x_train.index)
        x_train.rename(columns=columns, inplace=True)
        x_train['r'] = r
        y_train = x_train_y_train[PREDICTED_PARAMETER]
        dependent_variables = None
        if get_dependent_variables:
            dependent_variables = x_train_y_train[DEPENDENT_VARIABLES]
        print('mean train: ', np.mean(y_train))
        print('shapes: ', x_train.shape, y_train.shape)
        if get_scenario_uuid:
            x_train['scenario_uuid'] = df['scenario']
    else:
        x_train = y_train = None

    return x_train, y_train, dependent_variables


def normalize_list(value_list):
    xmin = min(value_list)
    xmax = max(value_list)
    for i, x in enumerate(value_list):
        value_list[i] = (x - xmin) / (xmax - xmin)
    return value_list


def plot_predicted_and_actual_results(y_predicted, y_actual, model_name=''):
    """
    Plots predicted and actual results after training a model on training data.
    :param y_predicted: predicted variables.
    :param y_actual: actual, expected variables.
    :param model_name: name of the trained model.
    :return: saves a plot as file.
    """
    plt.figure(figsize=(8, 6))
    x_ax = range(len(y_actual))
    sorted_tuples = sorted(zip(y_actual, y_predicted))
    y_actual = [x for x, y in sorted_tuples]
    y_predicted = [y for x, y in sorted_tuples]
    plt.scatter(x_ax, y_actual, label="original")
    plt.scatter(x_ax, y_predicted, label="predicted")
    # for x in x_ax:
    #   plt.plot([x, x], [y_actual[x], y_predicted[x]], 'gray', linewidth=0.5)
    plt.title(f"{model_name} performance test and predicted data")
    plt.ylabel('predicted and actual performance values')
    plt.xlabel('scenarios')
    plt.legend(loc='best', fancybox=True, shadow=True)
    plt.grid(True)

    util.directory_exists_or_create(dir_name='results/model_predictions')
    plt.savefig('../meta_optimization/results/model_predictions/' + str(model_name))
    plt.clf()


def calculate_scores(y_predicted, y_actual):
    """
    Calculates different error measurements.
    :param y_predicted: predicted variables.
    :param y_actual: actual, expected variables.
    :return: measurements as dict.
    """
    scores = dict()
    scores['r2_score'] = r2_score(y_actual, y_predicted)
    scores['mse'] = mean_squared_error(y_actual, y_predicted)
    scores['mae'] = mean_absolute_error(y_actual, y_predicted)
    scores['rmse'] = np.sqrt(mean_squared_error(y_actual, y_predicted))
    return scores


def do_permutation_feature_importance_analysis(model, x_test, y_test):
    """
    Executes permutation feature importance analysis for a trained model on test data.
    :param model: trained model.
    :param x_test: independent test variables.
    :param y_test: dependent test variables.
    :return: prints permutation importance and returns permutation importance.
    """
    perm = permutation_importance(model, x_test, y_test)

    for i in perm.importances_mean.argsort()[::-1]:
        if perm.importances_mean[i] - 2 * perm.importances_std[i] > 0:
            print(f"{x_test.columns[i]:<8}, {perm.importances_mean[i]:.3f} +/- {perm.importances_std[i]:.3f}")
    return perm


def predict_parameter(model_name, parameter, x_test, y_pred, y_test):
    parameter_pred = get_best_predicted_parameter(x_test, y_pred, parameter)[f'best_{parameter}'].tolist()
    parameter_actual = get_best_predicted_parameter(x_test, y_test, parameter)[f'best_{parameter}'].tolist()
    print(f'Scores for parameter {parameter} prediction: ')
    print(calculate_scores(parameter_pred, parameter_actual))
    plot_predicted_and_actual_best_parameter(model_name=model_name,
                                             predicted_intervals=parameter_pred,
                                             actual_intervals=parameter_actual,
                                             parameter=parameter)


def get_best_predicted_parameter(x, y, parameter):
    df = x.copy()
    variables = [short for long, short in columns.items()]
    feature_df = df[variables].drop(columns=['mean(r)']).drop_duplicates()
    df['y'] = y

    # get scenario by scenario
    for index, scenario in feature_df.iterrows():
        parameter_y_mapping = list()
        # check values in df and scenario
        for column_name in scenario.index:
            for _, row in df.iterrows():
                if row[column_name] == scenario[column_name]:
                    parameter_y_mapping.append((row[parameter], row['y']))
        minimum_y_tuple = min(parameter_y_mapping, key=lambda tup: tup[1])
        feature_df.at[index, f'best_{parameter}'] = minimum_y_tuple[0]
        feature_df.at[index, 'y'] = minimum_y_tuple[1]
    return feature_df


def plot_predicted_and_actual_best_parameter(model_name, predicted_intervals, actual_intervals, parameter):
    # Set up the plot
    plt.figure(figsize=(8, 6))
    sns.set_style('whitegrid')
    # Set up the custom purple color palette

    df = pd.DataFrame({parameter: actual_intervals, 'type': 'actual',
                       'scenario': [i for i in range(len(actual_intervals))]})
    df_predicted = pd.DataFrame({parameter: predicted_intervals, 'type': 'predicted',
                                 'scenario': [i for i in range(len(actual_intervals))]})

    df = df.append(df_predicted).reset_index()

    plt.ylim(-0.1, 1.1)

    sns.barplot(data=df, x='scenario', y=parameter, hue='type', palette='Blues')
    sns.lineplot(data=df, x='scenario', y=parameter, hue='type', palette='Blues')

    plt.xticks([])

    util.directory_exists_or_create(dir_name=f'/results/model_predictions/{parameter}_predictions/')
    plt.savefig(f'../meta_optimization/results/model_predictions/{parameter}_predictions/' + str(model_name))
    plt.clf()
    plt.close()
    """"# Create the scatter plot
    plt.scatter([i for i in range(len(predicted_intervals))], predicted_intervals, c=custom_palette[0],
                label=f'predicted best {parameter}')
    plt.scatter([i for i in range(len(actual_intervals))], actual_intervals, c=custom_palette[1],
                label=f'actual best {parameter}')

    # Add labels and title
    # plt.ylabel(parameter)
    plt.xlabel('scenarios')

    plt.ylim(-0.1, 1.1)

    # Add legend
    plt.legend()

    util.directory_exists_or_create(dir_name=f'../meta_optimization/results/model_predictions/{parameter}_predictions/')
    plt.savefig(f'../meta_optimization/results/model_predictions/{parameter}_predictions/' + str(model_name))
    plt.clf()
    plt.close()
    """