from mango.role.core import RoleAgent

from src.roles import FLAFlexibilityRole, ControllerRole


class FlexAgent(RoleAgent):
    """
    Agent responsible for flexibility related tasks.
    """
    def __init__(self, container, suggested_aid=None, neighborhood_definition=None, walk_definition=None,
                 model_configuration=None):
        super().__init__(container, suggested_aid=suggested_aid)
        self.add_role(FLAFlexibilityRole(neighborhood_definition=neighborhood_definition,
                                         walk_definition=walk_definition,
                                         model_configuration=model_configuration))


class ControllerAgent(RoleAgent):
    """
    Agent responsible for controlling tasks, such as aggregating the FLA-features and parametrizing COHDA.
    """
    def __init__(self, container, known_agents, suggested_aid=None):
        super().__init__(container, suggested_aid=suggested_aid)
        self.add_role(ControllerRole(known_agents=known_agents))

