import math
import random
import warnings
from copy import copy, deepcopy

import numpy as np

import src.definitions as definitions
from src.fla_flexibility_model import FLAFlexibilityModel
from pysimmods.other.flexibility.schedule import Schedule
from src.definitions import NeighborhoodDefinition, WalkDefinition, WalkType, LocalFeature

warnings.simplefilter(action='ignore', category=FutureWarning)


def _choose_neighbor(neighboring_solutions: list):
    """
    Choose neighbor in FLA dependent on the type of walk.
    :param neighboring_solutions: possible neighbors.
    :return: chosen neighbor.
    """
    if len(neighboring_solutions) == 0:
        print('No neighboring solutions found. ')
        return None
    return random.choice(neighboring_solutions)


def normalize_list(value_list):
    xmin = min(value_list)
    xmax = max(value_list)
    for i, x in enumerate(value_list):
        value_list[i] = (x - xmin) / (xmax - xmin)
    return value_list


class FlexFitnessLandscape:
    def __init__(self,
                 agent_id,
                 initial_schedule: Schedule = None,
                 other_schedules: list = None,
                 target_schedule: list = None,
                 fla_flexibility_model: FLAFlexibilityModel = None,
                 neighborhood_definition: NeighborhoodDefinition = None,
                 walk_definition: WalkDefinition = None
                 ):
        self._agent_id = agent_id
        # Schedules and flexibility calculation
        self._current_own_schedule = initial_schedule
        self._other_schedules = other_schedules
        self._fla_flexibility_model = fla_flexibility_model
        # Optimization
        self._objective = Objective(target_schedule=target_schedule)
        # FLA definitions
        self._neighborhood_definition = neighborhood_definition
        self._walk_definition = walk_definition
        self._walks = list()
        self._progressive_walk_starting_zone = None
        # Features
        self.information_content = None
        self.partial_information_content = None
        self.information_stability = None
        self.fitness_variance_mean = None
        self.fitness_variance_std = None
        self.dispersion_index = None
        self.fitness_mean = None
        self.mean_number_of_neutral_areas = None
        self.mean_size_of_neutral_areas = None
        self.mean_fitness_distance = None
        self.max_fitness_distance = None

    @property
    def walks(self):
        return self._walks

    def execute_walk(self):
        """
        Executes walk in flexibility fitness landscape and appends path as Walk object to list of walks.
        :return: nothing.
        """
        for random_walk_i in range(2):
            print('Execute random walk ', random_walk_i, ' for agent ', self._agent_id)
            schedules = self._other_schedules.copy()
            schedules.append(self._current_own_schedule)
            initial_operating_schedule_candidate = OperatingScheduleCandidate(schedules=schedules)
            self._objective.calculate_fitness(initial_operating_schedule_candidate)

            walk = Walk(agent_id=self._agent_id,
                        walk_type=WalkType.RANDOM,
                        initial_operating_schedule_candidate=initial_operating_schedule_candidate)
            for step in range(self._walk_definition.number_of_steps_per_walk):
                next_neighbor = None
                while not next_neighbor:
                    neighboring_solutions = self._fla_flexibility_model.get_neighboring_solutions_random(
                        neighborhood_definition_type=self._neighborhood_definition.neighborhood_definition_type,
                        neighborhood_distance_func=self._neighborhood_definition.distance_func,
                        neighborhood_max_distance=self._neighborhood_definition.max_distance,
                        current_own_schedule=self._current_own_schedule)
                    next_neighbor = _choose_neighbor(neighboring_solutions)
                    schedules = schedules[:-1]
                    self._current_own_schedule = next_neighbor
                    schedules.append(self._current_own_schedule)
                    new_operating_schedule_candidate = OperatingScheduleCandidate(schedules=deepcopy(schedules))
                    self._objective.calculate_fitness(new_operating_schedule_candidate)
                    walk.add_operating_schedule_candidate(new_operating_schedule_candidate)
                    del new_operating_schedule_candidate
            self._walks.append(walk)
        for progressive_walk_i in range(4):
            schedules = self._other_schedules.copy()
            schedules.append(self._current_own_schedule)
            initial_operating_schedule_candidate = OperatingScheduleCandidate(schedules=schedules)
            self._objective.calculate_fitness(initial_operating_schedule_candidate)

            walk = Walk(agent_id=self._agent_id,
                        walk_type=WalkType.PROGRESSIVE,
                        initial_operating_schedule_candidate=initial_operating_schedule_candidate)
            num_dimensions = len(self._current_own_schedule._data)
            # randomize starting zone
            self._progressive_walk_starting_zone = [random.choice([0, 1]) for i in range(num_dimensions)]
            print('Execute progressive walk ', progressive_walk_i,
                  ' with starting zone ', self._progressive_walk_starting_zone, ' for agent ', self._agent_id)
            for step in range(self._walk_definition.number_of_steps_per_walk):
                next_neighbor = self._choose_neighbor_with_progressive_walk()
                schedules = schedules[:-1]
                self._current_own_schedule = next_neighbor
                schedules.append(self._current_own_schedule)
                new_operating_schedule_candidate = OperatingScheduleCandidate(schedules=deepcopy(schedules))
                self._objective.calculate_fitness(new_operating_schedule_candidate)
                walk.add_operating_schedule_candidate(new_operating_schedule_candidate)
                del new_operating_schedule_candidate
            self._walks.append(walk)

    def _choose_neighbor_with_progressive_walk(self):
        num_dimensions = len(self._current_own_schedule._data)
        current_solution = copy(self._current_own_schedule)

        for i in range(num_dimensions):
            random_number = random.random() * self._walk_definition.step_size_factor \
                            * self._fla_flexibility_model.get_pn_max_kw()
            if self._progressive_walk_starting_zone[i] == 1:
                random_number *= -1
            current_solution, s_i = self._fla_flexibility_model.get_neighboring_solution_progressive(
                s_i=self._progressive_walk_starting_zone[i],
                current_solution=current_solution._data,
                index=i,
                random_number=random_number)
            self._progressive_walk_starting_zone[i] = s_i
        return current_solution

    def calculate_features(self, feature_df, features, col_index):
        """
        Calculate features of flexibility fitness landscape.
        :param feature_df: initial dataframe for features.
        :param features: FLA-features to calculate.
        :param col_index: index of column for adding information to dataframe.
        :return: updated dataframe.
        """
        if features:
            new_row = {'NeighborhoodDefinition': self._neighborhood_definition.neighborhood_definition_type.name,
                       'TargetSchedule': self._objective.target_schedule,
                       'Agent': self._agent_id, 'FLA_ID': col_index}
            for feature in features:
                if feature == LocalFeature.INFORMATION_CONTENT.name:
                    new_row['INFORMATION_CONTENT'] = self._calculate_information_content()
                elif feature == LocalFeature.PARTIAL_INFORMATION_CONTENT.name:
                    new_row['PARTIAL_INFORMATION_CONTENT'] = self._calculate_partial_information_content()
                elif feature == LocalFeature.INFORMATION_STABILITY.name:
                    new_row['INFORMATION_STABILITY'] = self._calculate_information_stability()
                elif feature == LocalFeature.FITNESS_VARIANCE_MEAN.name:
                    fitness_variance_mean, fitness_variance_std = self._calculate_fitness_variance()
                    new_row['FITNESS_VARIANCE_MEAN'] = fitness_variance_mean
                    new_row['FITNESS_VARIANCE_STD'] = fitness_variance_std
                elif feature == LocalFeature.DISPERSION_INDEX.name:
                    new_row['DISPERSION_INDEX'] = self._calculate_dispersion_index(
                        distance_func=self._neighborhood_definition.distance_func)
                elif feature == LocalFeature.FITNESS_MEAN.name:
                    new_row['FITNESS_MEAN'] = self._calculate_fitness_mean()
                elif feature == LocalFeature.MEAN_NUMBER_OF_NEUTRAL_AREAS.name:
                    new_row['MEAN_NUMBER_OF_NEUTRAL_AREAS'] = self._calculate_number_of_neutral_areas()
                elif feature == LocalFeature.MEAN_SIZE_OF_NEUTRAL_AREAS.name:
                    new_row['MEAN_SIZE_OF_NEUTRAL_AREAS'] = self._calculate_size_of_neutral_areas()
                elif feature == LocalFeature.MEAN_FITNESS_DISTANCE.name:
                    new_row['MEAN_FITNESS_DISTANCE'] = self._calculate_mean_fitness_distance()
                elif feature == LocalFeature.MAX_FITNESS_DISTANCE.name:
                    new_row['MAX_FITNESS_DISTANCE'] = self._calculate_max_fitness_distance()
            feature_df = feature_df.append(new_row, ignore_index=True)
        return feature_df

    def _calculate_number_of_neutral_areas(self):
        number_of_neutral_areas = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.RANDOM:
                number_of_neutral_areas.append(walk.get_number_of_neutral_areas())
        self.mean_number_of_neutral_areas = np.mean(number_of_neutral_areas)
        return self.mean_number_of_neutral_areas

    def _calculate_size_of_neutral_areas(self):
        size_of_neutral_areas = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.RANDOM:
                size_of_neutral_areas.append(walk.get_mean_size_of_neutral_areas())
        self.mean_size_of_neutral_areas = np.mean(size_of_neutral_areas)
        return self.mean_size_of_neutral_areas

    def _calculate_mean_fitness_distance(self):
        fitness_distances = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                fitness_distances.append(np.mean([abs(walk.fitness_values[i] - walk.fitness_values[i - 1])
                                                  for i in range(1, len(walk.fitness_values))]))
        self.mean_fitness_distance = np.mean(fitness_distances)
        return self.mean_fitness_distance

    def _calculate_max_fitness_distance(self):
        fitness_distances = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                fitness_distances.append(np.mean([abs(walk.fitness_values[i] - walk.fitness_values[i - 1])
                                                  for i in range(1, len(walk.fitness_values))]))
        self.max_fitness_distance = max(fitness_distances)
        return self.max_fitness_distance

    def _calculate_dispersion_index(self, distance_func):
        """
        The dispersion index quantifies distances between the best (10 percent) of solutions in the path and thus
        indicates the presence of funnels.
        A low value (< 0) indicates a single funnel landscape with an underlying uni-modal structure.
        A high value (> 0) indicates a multi-funnel landscape and underlying multi-modal structure.
        :param distance_func: function used for distance calculation in neighborhood.
        :return: dispersion index for path.
        """
        random_walks = [walk for walk in self._walks if walk.walk_type == WalkType.RANDOM]
        if len(random_walks) < 2:
            raise RuntimeError('Cannot calculate dispersion index.')
        best_solutions = [candidate.total_schedule.values for candidate in
                          sorted(random_walks[0].operating_schedule_candidates, key=lambda x: x.fitness)[
                          0:int(len(random_walks[0].operating_schedule_candidates) / 10)]]
        other_solutions = [candidate.total_schedule.values for candidate in
                           sorted(random_walks[0].operating_schedule_candidates,
                                  key=lambda x: x.fitness)]

        normalized_list_best_solutions = [normalize_list(solution) for solution in best_solutions]

        # calculate pairwise distances between normalized values
        distances_best_solutions = [distance_func(normalized_list_best_solutions[i],
                                                  normalized_list_best_solutions[i + 1])
                                    for i in range(len(normalized_list_best_solutions) - 1)]

        normalized_list_other_solutions = [normalize_list(solution) for solution in other_solutions]

        # calculate pairwise distances between normalized values
        distances_other_solutions = [distance_func(normalized_list_other_solutions[i],
                                                   normalized_list_other_solutions[i + 1])
                                     for i in range(len(normalized_list_other_solutions) - 1)]

        self.dispersion_index = np.mean(distances_best_solutions) - np.mean(distances_other_solutions)
        return self.dispersion_index

    def _calculate_fitness_variance(self):
        fitness_variances = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                fitness_variances.append(walk.get_fitness_variance())
        self.fitness_variance_mean = np.mean(fitness_variances)
        self.fitness_variance_std = np.std(fitness_variances)
        return self.fitness_variance_mean, self.fitness_variance_std

    def _calculate_fitness_mean(self):
        fitness_means = [np.mean(walk.fitness_values) for walk in self._walks if walk.walk_type == WalkType.PROGRESSIVE]
        self.fitness_mean = np.mean(fitness_means)
        return self.fitness_mean

    def _calculate_information_content(self):
        """
        The information content of a fitness landscape is calculated by taking the mean information content of all walks
        executed in the landscape.
        :return: mean information content of FlexFLA.
        """
        information_contents = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                information_contents.append(walk.get_information_content())
        self.information_content = np.mean(information_contents)
        return self.information_content

    def _calculate_partial_information_content(self):
        """
        The partial information content of a fitness landscape is calculated by taking the mean partial information
        content of all walks executed in the landscape.
        :return: mean partial information content of FlexFLA.
        """
        partial_information_contents = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                partial_information_contents.append(walk.get_partial_information_content())
        self.partial_information_content = np.mean(partial_information_contents)
        return self.partial_information_content

    def _calculate_information_stability(self):
        """
        The information stability of a fitness landscape is calculated by taking the mean information stability of all
        walks executed in the landscape.
        :return: mean information stability of FlexFLA.
        """
        information_stabilities = list()
        for walk in self._walks:
            if walk.walk_type == WalkType.PROGRESSIVE:
                information_stabilities.append(walk.get_information_stability())
        self.information_stability = np.mean(information_stabilities)
        return self.information_stability


class OperatingScheduleCandidate:
    """
    An OperatingScheduleCandidate holds a possible solution of an operating schedule of the agents in a MAS.
    """

    def __init__(self,
                 schedules: list):
        self._schedules = schedules
        self._total_schedule = None
        self._fitness = None

    @property
    def total_schedule(self):
        self._total_schedule = sum([schedule._data['p_kw'] for schedule in self._schedules])
        return self._total_schedule

    def set_fitness(self, fitness):
        self._fitness = fitness

    @property
    def fitness(self):
        return self._fitness


class Walk:
    def __init__(self,
                 agent_id: str,
                 walk_type: WalkType = None,
                 initial_operating_schedule_candidate: OperatingScheduleCandidate = None):
        self._agent_id = agent_id
        self.walk_type = walk_type
        self._operating_schedule_candidates = list()
        self._operating_schedule_candidates.append(initial_operating_schedule_candidate)
        self._entropy_string = None
        self._fitness_values = list()
        self._neutral_areas = list()

    @property
    def operating_schedule_candidates(self):
        return self._operating_schedule_candidates

    @property
    def fitness_values(self):
        if self._fitness_values:
            return self._fitness_values
        self._fitness_values = [operating_schedule_candidate.fitness for operating_schedule_candidate
                                in self._operating_schedule_candidates]
        return self._fitness_values

    @property
    def neutral_areas(self):
        if self._neutral_areas:
            return self._neutral_areas
        neutral_area_size = 0
        for i in range(1, len(self.fitness_values)):
            # if two fitness values are the same: is part of neutral area
            if abs(self.fitness_values[i] - self.fitness_values[i - 1]) <= definitions.Parameters.NEUTRAL_AREA_EPSILON:
                neutral_area_size += 1
            # if two fitness values are different: is not part of neutral area
            else:
                if neutral_area_size != 0:
                    # reached end of neutral area -> append current to list
                    self._neutral_areas.append(neutral_area_size)
                    neutral_area_size = 0
        return self._neutral_areas

    def get_number_of_neutral_areas(self):
        return len(self.neutral_areas)

    def get_mean_size_of_neutral_areas(self):
        if len(self.neutral_areas) == 0:
            return 0
        return np.mean(self.neutral_areas)

    def get_entropy_string(self, entropy_epsilon=None):
        """
        The entropy string is used in an information analysis of the landscape.
        The entropy sequence is a time series that represents a path in and contains information about the structure of
        the landscape.
        It can be defined as a string S with symbols s_1, s_2 ..., s_n out of {-1,0,1}.
        The differences between fitness values in a path are encoded as:
        1 if going uphill
        0 if no change
        -1 if going downhill
        :param entropy_epsilon: the threshold used for calculation.
        :return: entropy string containing symbols out of the set {-1,0,1}.
        """
        if self._entropy_string and not entropy_epsilon:
            return self._entropy_string
        if not entropy_epsilon:
            entropy_epsilon = definitions.Parameters.ENTROPY_EPSILON
        token_string = list()
        fitness_values = self.fitness_values
        for i in range(1, len(fitness_values)):
            if fitness_values[i] - fitness_values[i - 1] > entropy_epsilon:
                token_string.append(1)  # uphill
            elif abs(fitness_values[i] - fitness_values[i - 1]) <= entropy_epsilon:
                token_string.append(0)  # no change
            elif fitness_values[i] - fitness_values[i - 1] < -entropy_epsilon:
                token_string.append(-1)  # downhill
        if not entropy_epsilon:
            self._entropy_string = token_string
        return token_string

    def get_shortened_entropy_string(self):
        """
        An entropy string might be shortened by deleting all 0s and all areas with no changes.
        :return: shortened entropy string.
        """
        entropy_string = self.get_entropy_string()
        shortened_entropy_string = list()
        for i in range(1, len(entropy_string)):
            if entropy_string[i] != 0 and entropy_string[i - 1] != entropy_string[i]:
                shortened_entropy_string.append(entropy_string[i])
        return shortened_entropy_string

    def get_information_content(self):
        """
        The information content of a path generated by a walk is defined as the frequency of changes in the
        "FLA-direction" with respect to the length of the overall path.
        :return: the information content H(epsilon).
        """
        entropy_string = self.get_entropy_string()
        pq = 0
        for t_i in range(1, len(self.get_entropy_string())):
            if entropy_string[t_i - 1] != entropy_string[t_i]:
                pq += 1
        p_pq = pq / len(self.get_entropy_string())
        if all(value == 0 for value in entropy_string):
            return 0
        # frequency of different p and q in token string
        h = - p_pq * math.log(p_pq, 6)
        return h

    def get_partial_information_content(self):
        """
        The partial information content relates to the modality encountered on the landscape path.
        :return: partial information content.
        """
        return len(self.get_shortened_entropy_string()) / len(self.get_entropy_string())

    def get_information_stability(self):
        """
        The information stability is defined as the highest fitness difference between neighboring points reached in the
        entropy sting series.
        :return: information stability.
        """
        entropy_epsilon = definitions.Parameters.ENTROPY_EPSILON
        while not all([x == 0 for x in self.get_entropy_string(entropy_epsilon)]):
            entropy_epsilon += definitions.Parameters.INCREMENT_ENTROPY_EPSILON
        return entropy_epsilon

    def get_fitness_variance(self):
        fitness_values = self.fitness_values

        # Calculate the mean and standard deviation of the dataset
        mean_value = np.mean(fitness_values)
        max_value = np.max(fitness_values)
        min_value = np.min(fitness_values)

        if max_value == min_value:
            return 0
        # Mean normalization
        normalized_fitness_values = [(fitness_value - mean_value) / (max_value - min_value)
                                     for fitness_value in fitness_values]

        return np.var(normalized_fitness_values)

    def add_operating_schedule_candidate(self, operating_schedule_candidate):
        """
        Add object of type OperatingScheduleCandidate to list of candidates in walk.
        :param operating_schedule_candidate: OperatingScheduleCandidate along the path generated in the walk.
        :return: nothing.
        """
        self._operating_schedule_candidates.append(operating_schedule_candidate)

    def get_number_of_unique_local_minima(self):
        """
        Calculates the set of of unique local minima in path of walk.
        :return: list of unique local minima.
        """
        return [t for t in (set(tuple(i) for i in self.get_local_minima_with_total_schedule()))]

    def get_local_minima_with_total_schedule(self):
        """
        Returns all local minima along the path with information about the fitness of minima and the operating schedule.
        :return: minima by: (operating schedule candidate, fitness value).
        """
        fitness_values = self.fitness_values
        local_minima = [(self._operating_schedule_candidates[i], y) for i, y in enumerate(fitness_values)
                        if ((i == 0) or (fitness_values[i - 1] >= y))
                        and ((i == len(fitness_values) - 1) or (y < fitness_values[i + 1]))]
        return local_minima

    def get_local_minima(self):
        """
        Returns all local minima along the path with information about the fitness of minima.
        :return: minima.
        """
        fitness_values = self.fitness_values
        local_minima = [(i, y) for i, y in enumerate(fitness_values)
                        if ((i == 0) or (fitness_values[i - 1] >= y))
                        and ((i == len(fitness_values) - 1) or (y < fitness_values[i + 1]))]
        return local_minima


class Objective:
    def __init__(self,
                 target_schedule: list = None):
        self._target_schedule = target_schedule

    @property
    def target_schedule(self):
        return self._target_schedule

    def calculate_fitness(self, operating_schedule_candidate: OperatingScheduleCandidate = None):
        # deviation to the target schedule
        diff = np.abs(np.array(self._target_schedule) - operating_schedule_candidate.total_schedule)
        fitness = np.sum(diff)
        operating_schedule_candidate.set_fitness(fitness)
        return fitness
