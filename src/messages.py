from mango.messages.codecs import json_serializable


@json_serializable
class FlexRequest:
    """Message for requesting flexibility information from agents.
    """

    def __init__(self, start_date=None, flex_start=None, other_agents=None):
        if other_agents is None:
            other_agents = list()
        self._start_date = start_date
        self._flex_start = flex_start
        self._other_agents = other_agents

    @property
    def start_date(self):
        return self._start_date

    @property
    def flex_start(self):
        return self._flex_start

    @property
    def other_agents(self):
        return self._other_agents


@json_serializable
class FlexRequestAck:
    """Message for acknowledging flexibility information from agents.
    """

    def __init__(self, start_date=None, flex_start=None, flexibilities=None):
        self._start_date = start_date
        self._flex_start = flex_start
        self._flexibilities = flexibilities

    @property
    def start_date(self):
        return self._start_date

    @property
    def flex_start(self):
        return self._flex_start

    @property
    def flexibilities(self):
        return self._flexibilities


@json_serializable
class FlexibilitySample:
    """Message for acknowledging flexibility information from agents.
    """

    def __init__(self, start_date=None, flex_start=None, flexibilities=None):
        self._start_date = start_date
        self._flex_start = flex_start
        self._flexibilities = flexibilities

    @property
    def start_date(self):
        return self._start_date

    @property
    def flex_start(self):
        return self._flex_start

    @property
    def flexibilities(self):
        return self._flexibilities


@json_serializable
class FeatureRequest:
    """Message for requesting Flex-FLA features from agents.
    """

    def __init__(self, start_date=None, flex_start=None, target=None, features=None):
        self._start_date = start_date
        self._flex_start = flex_start
        self._target = target
        self._features = features

    @property
    def start_date(self):
        return self._start_date

    @property
    def flex_start(self):
        return self._flex_start

    @property
    def target(self):
        return self._target

    @property
    def features(self):
        return self._features


@json_serializable
class FeatureInformation:
    """
    Message for sending information about FLA-feature to Controller.
    """

    def __init__(self, features):
        self._features = features

    @property
    def features(self):
        return self._features


@json_serializable
class FeatureInformationAck:
    """
    Message for sending acknowledgment about feature information from Controller to agents.
    """
    def __init__(self):
        pass
