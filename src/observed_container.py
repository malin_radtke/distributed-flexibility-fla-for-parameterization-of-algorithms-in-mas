import asyncio
import random
from abc import ABC
from codecs import Codec
from typing import Union, Tuple, Optional, Dict, Any

from mango.core.container import Container, TCPContainer
from mango.core.container_protocols import ContainerProtocol
from mango.messages.codecs import JSON
from mango.util.clock import Clock, AsyncioClock


class ObservedContainer(Container, ABC):

    def __init__(self, *, addr, name: str, codec, loop, clock: Clock):
        super().__init__(addr=addr, name=name, codec=codec, loop=loop, clock=clock)
        self._messages_sent = 0

    @classmethod
    async def factory(
            cls,
            *,
            connection_type: str = "tcp",
            codec: Codec = None,
            clock: Clock = None,
            addr: Optional[Union[str, Tuple[str, int]]] = None,
            proto_msgs_module=None,
            mqtt_kwargs: Dict[str, Any] = None,
            send_messages_delayed=False,
    ):
        connection_type = connection_type.lower()
        if connection_type not in ["tcp", "mqtt"]:
            raise ValueError(f"Unknown connection type {connection_type}")

        loop = asyncio.get_running_loop()

        if codec is None:
            codec = JSON()
        if clock is None:
            clock = AsyncioClock()

        if connection_type == "tcp":
            # initialize TCPContainer
            container = ObservedTCPContainer(
                addr=addr, codec=codec, loop=loop, proto_msgs_module=proto_msgs_module, clock=clock,
                send_messages_delayed=send_messages_delayed
            )

            # create a TCP server bound to host and port that uses the
            # specified protocol
            container.server = await loop.create_server(
                lambda: ContainerProtocol(container=container, loop=loop, codec=codec),
                addr[0],
                addr[1],
            )
            return container


class ObservedTCPContainer(TCPContainer):

    def __init__(self,
                 *,
                 addr: Tuple[str, int],
                 codec: Codec,
                 loop: asyncio.AbstractEventLoop,
                 proto_msgs_module=None,
                 clock: Clock,
                 send_messages_delayed: bool):
        super().__init__(addr=addr,
                         codec=codec,
                         proto_msgs_module=proto_msgs_module,
                         loop=loop,
                         clock=clock, )
        self._send_messages_delayed = send_messages_delayed
        self._messages_sent = 0

    @property
    def messages_sent(self):
        return self._messages_sent

    async def send_message_delayed(self, content, receiver_addr, receiver_id,
                                   create_acl, acl_metadata, mqtt_kwargs,
                                   kwargs):
        if self._send_messages_delayed:
            delay = random.random() / 100
            await asyncio.sleep(delay)
        return await super().send_message(content=content, receiver_addr=receiver_addr, receiver_id=receiver_id,
                                          create_acl=create_acl, acl_metadata=acl_metadata, mqtt_kwargs=mqtt_kwargs,
                                          kwargs=kwargs)

    async def send_message(
            self,
            content,
            receiver_addr: Union[str, Tuple[str, int]],
            *,
            receiver_id: Optional[str] = None,
            create_acl: bool = None,
            acl_metadata: Optional[Dict[str, Any]] = None,
            mqtt_kwargs: Dict[str, Any] = None,
            **kwargs
    ) -> bool:
        self._messages_sent += 1
        return await asyncio.create_task(
            self.send_message_delayed(content=content, receiver_addr=receiver_addr, receiver_id=receiver_id,
                                      create_acl=create_acl, acl_metadata=acl_metadata, mqtt_kwargs=mqtt_kwargs,
                                      kwargs=kwargs))
