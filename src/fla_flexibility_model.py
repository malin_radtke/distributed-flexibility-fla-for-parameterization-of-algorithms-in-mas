import random
from datetime import datetime
from typing import Optional
import pandas as pd
import numpy as np

import src.definitions as definitions
from src.definitions import NeighborhoodDefinitionType
from pysimmods.other.flexibility.flexibility_model import FlexibilityModel
from pysimmods.other.flexibility.schedule import Schedule


def datetime_to_index(dt: datetime, step_size: int = 900, steps_per_year: int = 35135) -> int:
    dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
    dif = dif.total_seconds()
    idx = int(dif // step_size) % steps_per_year

    return idx


class FLAFlexibilityModel(FlexibilityModel):
    """
    Flexibility model with util functions for fitness landscape analysis.
    """
    def __init__(self,
                 model,
                 model_type: definitions.ModelType = None,
                 unit="kw",
                 prioritize_setpoint: bool = False,
                 step_size: Optional[int] = None,
                 now_dt: Optional[datetime] = None,
                 forecast_horizon_hours=1,
                 seed=None,
                 store_min_and_max: bool = False
                 ):
        super().__init__(model, unit, prioritize_setpoint, step_size, now_dt, forecast_horizon_hours, seed,
                         store_min_and_max)
        self.start_date = definitions.Constants.START_DATE
        self.flex_start = definitions.Constants.FLEX_START
        self.current_own_schedule = None
        self._flexibility_schedules = list()
        self._model_type = model_type

    @property
    def flexibility_schedules(self):
        return super().flexibilities._schedules()

    def get_neighboring_solutions_random(self, neighborhood_definition_type, neighborhood_distance_func,
                                         neighborhood_max_distance, current_own_schedule):
        """
        Method that returns neighboring solutions for random walk in fitness landscape
        dependent on the neighborhood definition.
        :param neighborhood_definition_type:
        :param neighborhood_distance_func:
        :param neighborhood_max_distance:
        :param current_own_schedule:
        :return:
        """
        self.current_own_schedule = current_own_schedule
        if neighborhood_definition_type is NeighborhoodDefinitionType.DISTANCE:
            pass
        if neighborhood_definition_type is NeighborhoodDefinitionType.SINGLE_CHANGE:
            FlexibilityModel.sample = self.sample_for_single_change
        flexibilities = self.generate_flexibilities()
        neighboring_solutions = list()
        while len(neighboring_solutions) == 0:
            for schedule in flexibilities._schedules.values():
                distance = neighborhood_distance_func(schedule._data['p_kw'], current_own_schedule._data['p_kw'])
                if distance <= neighborhood_max_distance:
                    neighboring_solutions.append(schedule)
            neighborhood_max_distance += 100
        return neighboring_solutions

    def get_neighboring_solution_progressive(self, s_i, current_solution, index, random_number):
        """
        Method that returns neighboring solution for progressive walk in fitness landscape.
        :param s_i: starting zone at dimension i.
        :param current_solution: current solution in walk.
        :param index: index in solution.
        :param random_number: random number to increase or decrease value at dimension.
        :return: (solution as Schedule, new starting zone)
        """
        index = current_solution.index[index]
        # add random value in given direction at specified index
        p_set_value = current_solution[self._psetname][index] + random_number
        # check if value is in given bounds
        if p_set_value <= self.get_pn_min_kw():
            p_set_value = 2 * self.get_pn_min_kw() - p_set_value
            s_i = 0
        elif p_set_value >= self.get_pn_max_kw():
            p_set_value = 2 * self.get_pn_max_kw() - p_set_value
            s_i = 1

        current_solution.loc[index, self._psetname] = p_set_value

        # test if it is feasible
        state_backup = self._model.get_state()

        # generate schedule
        for index, row in current_solution.iterrows():
            try:
                self._calculate_step(
                    index, row[self._psetname], row[self._qsetname]
                )
                current_solution.loc[index, self._pname] = (
                        self._model.get_p_kw() * self._unit_factor
                )
                current_solution.loc[index, self._qname] = (
                        self._model.get_q_kvar() * self._unit_factor
                )

            except KeyError:
                # Forecast is missing
                current_solution.loc[index, self._pname] = np.nan
                current_solution.loc[index, self._qname] = np.nan
                current_solution.loc[index, self._psetname] = np.nan
                current_solution.loc[index, self._qsetname] = np.nan
        self._model.set_state(state_backup)
        return Schedule().from_dataframe(current_solution), s_i

    def sample_for_single_change(self, index):
        """
        Overwrites 'normal' sampling for SINGLE_CHANGE neighborhood definition.
        :param index:
        :return:
        """
        dataframe = pd.DataFrame(
            columns=[
                self._psetname,
                self._qsetname,
                self._pname,
                self._qname,
            ],
            index=index,
        )
        change_value_at_index = random.randint(0, len(index) - 1)
        dataframe[self._psetname] = self.current_own_schedule._data[self._pname]
        dataframe[self._qsetname] = self.current_own_schedule._data[self._qname]
        dataframe[self._psetname][change_value_at_index] = self._rng.uniform(low=self.get_pn_min_kw(),
                                                                             high=self.get_pn_max_kw(),
                                                                             size=1,
                                                                             )[0]
        dataframe[self._qsetname][change_value_at_index] = self._rng.uniform(low=self.get_qn_min_kvar(),
                                                                             high=self.get_qn_max_kvar(),
                                                                             size=1,
                                                                             )[0]

        state_backup = self._model.get_state()
        for index, row in dataframe.iterrows():
            try:
                self._calculate_step(
                    index, row[self._psetname], row[self._qsetname]
                )
                dataframe.loc[index, self._pname] = (
                        self._model.get_p_kw() * self._unit_factor
                )
                dataframe.loc[index, self._qname] = (
                        self._model.get_q_kvar() * self._unit_factor
                )

            except KeyError:
                # Forecast is missing
                dataframe.loc[index, self._pname] = np.nan
                dataframe.loc[index, self._qname] = np.nan
                dataframe.loc[index, self._psetname] = np.nan
                dataframe.loc[index, self._qsetname] = np.nan
        self._model.set_state(state_backup)

        return Schedule().from_dataframe(dataframe)

    def generate_flexibilities(self, number_of_schedules=definitions.Parameters.NUMBER_OF_SCHEDULES):
        """
        Generates flexibilities in form of number_of_schedules Schedule objects.
        :param number_of_schedules: number of schedules to provide.
        :return: Flexibility object.
        """
        self.set_now_dt(self.start_date)
        self.set_step_size(15 * 60)
        self.set_p_kw(500)
        if self._model_type == definitions.ModelType.COMBINED_POWER_AND_HEAT or \
                self._model_type == definitions.ModelType.PHOTOVOLTAIC:
            widx = datetime_to_index(definitions.Constants.START_DATE_DT)
            wd = pd.read_csv(definitions.Constants.WD_PATH, index_col=0)
            self._model.inputs.day_avg_t_air_deg_celsius = wd.iloc[widx][definitions.Constants.T_AIR]
        if self._model_type == definitions.ModelType.PHOTOVOLTAIC:
            self._model.inputs.t_air_deg_celsius = wd.iloc[widx][definitions.Constants.T_AIR]
            self._model.inputs.bh_w_per_m2 = wd.iloc[widx][definitions.Constants.BH]
            self._model.inputs.dh_w_per_m2 = wd.iloc[widx][definitions.Constants.DH]

        self.step()

        flexibilities = super().generate_schedules(self.flex_start, 2, number_of_schedules)

        return flexibilities
