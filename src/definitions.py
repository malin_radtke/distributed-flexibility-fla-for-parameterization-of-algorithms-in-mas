from datetime import datetime, timezone
from enum import Enum
from typing import Optional
import os


class Constants:
    """
    Constants are fixed for one scenario.
    """
    # uuid in order to identify the scenario configuration
    SCENARIO_UUID = ''

    # target schedule in COHDA negotiation
    TARGET_SCHEDULE = None

    # constants for flexibility model
    T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
    BH = "WeatherCurrent__0___bh_w_per_m2"
    DH = "WeatherCurrent__0___dh_w_per_m2"

    # weather data
    WD_PATH = os.path.abspath(
        os.path.join(__file__, "..", "..", "data", "weather-time-series.csv")
    )

    # dates used for the flexibility calculation
    START_DATE = "2021-01-06 12:00:00+0100"
    START_DATE_DT = datetime(2021, 1, 8, 0, 0, 0, tzinfo=timezone.utc)
    FLEX_START = "2021-01-06 12:30:00+0100"


class NeighborhoodDefinitionType(Enum):
    """
    Neighborhoods between solution might be defined different in fitness landscapes.
    The different types are described as objects of the enum NeighborhoodDefinitionType.
    NeighborhoodDefinitionTypes are not relevant if progressive WalkType is chosen.
    """
    DISTANCE = 1
    SINGLE_CHANGE = 2


class WalkType(Enum):
    """
    Walks in fitness landscapes might be defined different.
    The different types are described as objects of the enum WalkType.
    For RANDOM walks the NeighborhoodDefinitionType has to be specified.
    """
    RANDOM = 1
    PROGRESSIVE = 2


class LocalFeature(Enum):
    """
    Different features might be calculated in terms of fitness landscape analysis.
    The different features are described as objects of the enum Feature.
    """
    INFORMATION_CONTENT = 1
    PARTIAL_INFORMATION_CONTENT = 2
    INFORMATION_STABILITY = 3
    FITNESS_VARIANCE_MEAN = 4
    FITNESS_VARIANCE_STD = 5
    FITNESS_MEAN = 6
    DISPERSION_INDEX = 7
    MEAN_NUMBER_OF_NEUTRAL_AREAS = 8
    MEAN_SIZE_OF_NEUTRAL_AREAS = 9
    MEAN_FITNESS_DISTANCE = 10
    MAX_FITNESS_DISTANCE = 11


class ModelType(Enum):
    """
    The energy models might have different types (consumer and producer) with different flexibility and power
    specifications.
    The different types are described as objects of the enum ModelType.
    """
    BATTERY = 1
    COMBINED_POWER_AND_HEAT = 2
    PHOTOVOLTAIC = 3
    DUMMY = 4


class Parameters:
    """
    Parameters that are not to be changed in different scenarios.
    """
    ENTROPY_EPSILON = 1
    NEUTRAL_AREA_EPSILON = 0.01
    INCREMENT_ENTROPY_EPSILON = 1
    NUMBER_OF_SCHEDULES = 50
    NUMBER_OF_LANDSCAPES = 5


class NeighborhoodDefinition:
    """
    Neighborhoods between solutions might be defined different in fitness landscapes.
    The class NeighborhoodDefinition holds the parameters that specify the definition of the neighborhood.
    """

    def __init__(self,
                 neighborhood_definition_type: NeighborhoodDefinitionType = NeighborhoodDefinitionType.DISTANCE,
                 max_distance: Optional[float] = None,
                 distance_func: Optional = None):
        self._neighborhood_definition_type = neighborhood_definition_type
        self._max_distance = max_distance
        self._distance_func = distance_func

    @property
    def neighborhood_definition_type(self):
        """
        Getter function for neighborhood definition type.
        :return: neighborhood definition type.
        """
        return self._neighborhood_definition_type

    @property
    def distance_func(self):
        """
        Getter function for distance function (only needed for type DISTANCE).
        :return: distance function.
        """
        return self._distance_func

    @property
    def max_distance(self):
        """
        Getter function for maximum distance (only needed for type DISTANCE).
        :return: maximum distance between solutions to be defined as neighbors.
        """
        return self._max_distance


class WalkDefinition:
    """
    Walks might be defined different in fitness landscapes.
    The class WalkDefinition holds the parameters that specify the definition of the walk.
    """

    def __init__(self,
                 number_of_steps_per_walk: int = 10,
                 step_size_factor: float = 1.0):
        self._number_of_steps_per_walk = number_of_steps_per_walk
        self._step_size_factor = step_size_factor

    @property
    def number_of_steps_per_walk(self):
        """
        Getter function for number of steps per walk.
        :return: number of steps per walk.
        """
        return self._number_of_steps_per_walk

    @property
    def step_size_factor(self):
        """
        Getter function for step size factor (only needed for type PROGRESSIVE).
        The factor is multiplied with the maximum power value of the flexibility model.
        :return: step size factor.
        """
        return self._step_size_factor
