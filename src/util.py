import json
import seaborn as sns
import matplotlib.pyplot as plt
from collections import Counter
import pandas as pd
import os
import numpy as np

from src.definitions import LocalFeature


def directory_exists_or_create(dir_name):
    # Check whether the specified path exists or not
    isExist = os.path.exists(dir_name)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(dir_name)
        print(f"The new directory {dir_name} is created!")


def plot_model_presets(model_presets, id):
    fig, ax = plt.subplots()

    x_labels = ['S', 'M', 'L']
    y_labels = ['PV', 'Battery', 'CHP']

    x_to_num = {p[1]: p[0] for p in enumerate(x_labels)}
    y_to_num = {p[1]: p[0] for p in enumerate(y_labels)}

    label_mapping = {
        'COMBINED_POWER_AND_HEAT': 0,
        'BATTERY': 1,
        'PHOTOVOLTAIC': 2
    }

    counter = Counter((x['size'], x['type']) for x in model_presets)
    x_values = [item[0][0] for item in counter.items()]
    y_values = [label_mapping[item[0][1].name] for item in counter.items()]
    size_scale = 700
    sizes = [item[1] * size_scale for item in counter.items()]

    # Define the colors for each category
    colors = ['#8d3c53', '#6a3863', '#a65764']

    # Use the colors list to map to each category
    c = [colors[i] for i in y_values]

    ax.scatter(
        x=x_values,
        y=y_values,
        marker='s',
        s=sizes,
        c=c
    )
    ax.set_xticks([x_to_num[v] for v in x_labels])
    ax.set_xticklabels(x_labels, horizontalalignment='right')
    ax.set_yticks([y_to_num[v] for v in y_labels])
    ax.set_yticklabels(y_labels)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    ax.tick_params(axis='both', length=0, width=0)

    ax.set_xlim([-0.5, max([v for v in x_to_num.values()]) + 0.5])
    ax.set_ylim([-0.5, max([v for v in y_to_num.values()]) + 0.5])

    directory_exists_or_create('asset_plots')
    plt.savefig(f'asset_plots/{id}')

    plt.clf()


def plot_metrics(filename_dict):
    sns.set()

    plt.rcParams["mathtext.fontset"] = "cm"

    mapping = {
        'INFORMATION_CONTENT': r'$H(\epsilon)$',
        'PARTIAL_INFORMATION_CONTENT': r'$M(\epsilon)$',
        'INFORMATION_STABILITY': r'$\epsilon*$',
        'FITNESS_VARIANCE_MEAN': r'$mean(var(f))$',
        'FITNESS_VARIANCE_STD': r'$std(var(f))$',
        'FITNESS_MEAN': r'$mean(f)$',
        'DISPERSION_INDEX': r'$DI$',
        'MEAN_NUMBER_OF_NEUTRAL_AREAS': r'$mean(num_{na})$',
        'MEAN_SIZE_OF_NEUTRAL_AREAS': r'$mean(size_{na})$',
        'MEAN_FITNESS_DISTANCE': r'$mean(dist_f)$',
        'MAX_FITNESS_DISTANCE': r'$max(dist_f)$'
    }

    # Set the figure size and number of subplots
    fig, axes = plt.subplots(nrows=len(filename_dict.values()), ncols=4, figsize=(15, 16),
                             gridspec_kw={'width_ratios': [2, 1, 2, 2]})

    # Iterate over each filename and corresponding subplot
    for filename, ax in zip(filename_dict.values(), axes):
        # Read the CSV file
        data = pd.read_csv(filename)

        data.rename(columns=mapping, inplace=True)

        information_analysis = [r'$H(\epsilon)$', r'$M(\epsilon)$']

        fitness = [r'$mean(f)$']

        distance = [r'$mean(dist_f)$', r'$max(dist_f)$']

        neutral_ares = [r'$mean(num_{na})$', r'$mean(size_{na})$',]

        sns.boxplot(data[information_analysis], palette='Blues', ax=ax[0])
        sns.boxplot(data[fitness], palette='Blues', ax=ax[1])
        sns.boxplot(data[distance], palette='Blues', ax=ax[2])
        sns.boxplot(data[neutral_ares], palette='Blues', ax=ax[3])
        for i in range(4):
            ax[i].tick_params(axis='x', which='major', labelsize=18, rotation=20)

        key = [k for k, v in filename_dict.items() if v == filename][0]

        ax[0].set_ylabel(key, size=18)  # Set the title for each subplot

        ax[0].set_ylim(-0.1, 0.3)

        # ax[1].set_ylim(0, 8)

        ax[2].set_ylim(-0.1, 1.8)

        ax[3].set_ylim(0, 28)

    # Adjust the spacing between subplots
    plt.subplots_adjust(wspace=0.1, hspace=0.567, right=0.97, left=0.05, top=0.96)

    # Show the plot
    plt.show()


def plot_schedule(path):
    # Read the JSON file
    with open(path, 'r') as file:
        data = json.load(file)

    for entry in data.values():
        p_kw_values = list(entry['p_kw'].values())

        # Create x-axis values (timestamps as difference from the smallest timestamp)
        timestamps = list(entry['p_kw'].keys())
        smallest_timestamp = min(timestamps)
        timestamps = [(int(timestamp) - int(smallest_timestamp)) / 1e9 for timestamp in timestamps]

        # Set seaborn style
        sns.set()
        sns.set(font_scale=2)

        # Plot the p_kw values
        sns.lineplot(x=timestamps, y=p_kw_values)

    plt.subplots_adjust(hspace=0.3, left=0.2, top=0.96)
    plt.xlabel('Time horizon')
    plt.xticks([])
    plt.ylabel('active power in kW')
    plt.show()


if __name__ == "__main__":
    path = '../data/flex_schedules_scenarios/chp.json'
    #plot_schedule(path)
    file = '../data/scenario_data_mixed/6d78b7b9-a4f5-4c9d-b297-378fd61b8fca.csv'
    filenames = {'PV only': '../data/scenario_data_pv_only/cc2d1667-210d-4dff-b08f-376a5de7d83f.csv',
                 'Battery only': '../data/scenario_data_battery_only/10fef63f-cf1d-4ba1-a051-ea1f2c6a657d.csv',
                 'CHP only': '../data/scenario_data_chp_only/51f08857-8533-4002-a4a7-dae0ed525c0b.csv',
                 'mixed': '../data/scenario_data_mixed/6d78b7b9-a4f5-4c9d-b297-378fd61b8fca.csv'}
    plot_metrics(filenames)
