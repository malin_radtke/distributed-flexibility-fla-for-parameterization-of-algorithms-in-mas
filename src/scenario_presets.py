from src.definitions import ModelType

model_presets = [
    {
        'type': ModelType.PHOTOVOLTAIC,
        'p_kw_peak': 8,
        'size': 0
    },
    {
        'type': ModelType.PHOTOVOLTAIC,
        'p_kw_peak': 10,
        'size': 1
    },
    {
        'type': ModelType.PHOTOVOLTAIC,
        'p_kw_peak': 15,
        'size': 2
    },
    {
        'type': ModelType.BATTERY,
        'pn_max_kw': 4,
        'size': 0
    },
    {
        'type': ModelType.BATTERY,
        'pn_max_kw': 5,
        'size': 1
    },
    {
        'type': ModelType.BATTERY,
        'pn_max_kw': 6,
        'size': 2
    },
    {
        'type': ModelType.COMBINED_POWER_AND_HEAT,
        'p_max_kw': 7,
        'size': 0
    },
    {
        'type': ModelType.COMBINED_POWER_AND_HEAT,
        'p_max_kw': 14,
        'size': 1
    },
    {
        'type': ModelType.COMBINED_POWER_AND_HEAT,
        'p_max_kw': 200,
        'size': 2
    }
]

start_date_presets = [
    {'start_date': "2021-07-01 12:00:00+0100",
     'flex_start': "2021-07-01 12:30:00+0100"}
]
