import asyncio
import os
import random

import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import qmc
from mango.role.api import Role

import src.definitions as definitions
import src.util as util
from pysimmods.buffer.batterysim import Battery
from pysimmods.buffer.batterysim.presets import battery_preset
from pysimmods.generator.chplpgsystemsim import CHPLPGSystem
from pysimmods.generator.chplpgsystemsim.presets import chp_preset
from pysimmods.generator.pvsystemsim import PVPlantSystem
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.other.flexibility.flexibilities import Flexibilities
from pysimmods.other.dummy.model import DummyModel

from src.definitions import NeighborhoodDefinition, WalkDefinition, LocalFeature
from src.fla_flexibility_model import FLAFlexibilityModel
from src.flex_fitness_landscape import FlexFitnessLandscape
from src.messages import FlexRequestAck, FlexRequest, FlexibilitySample, FeatureRequest, FeatureInformation, \
    FeatureInformationAck


class FLAFlexibilityRole(Role):
    """
    Role responsible for analyzing flexibility of an energy plant with help of a fitness landscape.
    """

    def __init__(self,
                 neighborhood_definition: NeighborhoodDefinition = None,
                 walk_definition: WalkDefinition = None,
                 model_configuration=None):
        super().__init__()
        self.acl_meta = None
        self._controller_addr = None
        self._controller_id = None
        self._flexibility_samples = None
        self._fla_flexibility_model = None
        self._model_configuration = model_configuration
        self._neighborhood_definition = neighborhood_definition
        self._walk_definition = walk_definition
        self._flexibility_fitness_landscapes = list()
        self._received_feature_information_ack = asyncio.Future()

    def setup(self) -> None:
        """
        Set up role -> therefore, subscribe messages.
        :return: nothing.
        """
        self.context.subscribe_message(self, self._handle_flexibility_request,
                                       lambda content, meta: isinstance(content, FlexRequest))
        self.context.subscribe_message(self, self._handle_flexibility_sample,
                                       lambda content, meta: isinstance(content, FlexibilitySample))
        self.context.subscribe_message(self, self._handle_feature_request,
                                       lambda content, meta: isinstance(content, FeatureRequest))
        self.context.subscribe_message(self, self._handle_feature_information_ack,
                                       lambda content, meta: isinstance(content, FeatureInformationAck))
        self.acl_meta = {"sender_addr": self.context.addr, "sender_id": self.context.aid}

    def _handle_feature_information_ack(self, content, meta):
        """
        Handle method for message of type FeatureInformationAck.
        Sets asyncio.Future() to True.
        :param content:
        :param meta:
        :return:
        """
        print(self.context.aid, ' received feature information ack.')
        self._received_feature_information_ack.set_result(True)

    def _handle_flexibility_request(self, content, meta):
        """
        Method is called if agent role receives a message of the type FlexRequest from the controller.
        Now, send a FlexRequestAck towards the controller, generate own flexibility and send sample to other agents.
        :param content: message content.
        :param meta: message meta.
        :return: nothing.
        """
        # send FlexibilityRequestAck to controller
        self._controller_addr = (meta['sender_addr'][0], meta['sender_addr'][1])
        self._controller_id = meta['sender_id']

        start_date = content.start_date
        flex_start = content.flex_start
        other_agents = content.other_agents
        self._flexibility_samples = dict.fromkeys([agent[1] for agent in other_agents
                                                   if agent[1] != self.context.aid])

        self.generate_fla_flexibility_model()
        flexibilities = self._fla_flexibility_model.generate_flexibilities()

        self.context.schedule_instant_task(
            self.context.send_acl_message(
                content=FlexRequestAck(start_date=content.start_date, flex_start=content.flex_start,
                                       flexibilities=flexibilities.to_json()),
                receiver_addr=self._controller_addr,
                receiver_id=self._controller_id,
                acl_metadata=self.acl_meta,
            )
        )

        self.write_flexibilities_to_json_file(flexibilities)
        self._send_flexibilities_to_other_agents(start_date, flex_start, flexibilities, other_agents)

    def write_flexibilities_to_json_file(self, flexibilities):
        """
        The flexibilities of the agents are used later for the negotiation with COHDA.
        Therefore, the schedules are saved in a file.
        :param flexibilities: Flexibility object.
        :return: nothing.
        """
        flex_json_object = flexibilities.to_json()
        # Writing to sample.json
        util.directory_exists_or_create('data/flexibility_schedules')
        with open(f"data/flexibility_schedules/{self.context.aid}.json", "w") as outfile:
            outfile.write(flex_json_object)

    def generate_fla_flexibility_model(self):
        """
        Different types of models are possible.
        In this method the given type of model is generated as a FLAFlexibilityModel.
        :return: nothing.
        """
        model_type = self._model_configuration['type']
        if model_type == definitions.ModelType.DUMMY:
            self._fla_flexibility_model = FLAFlexibilityModel(DummyModel(dict(), dict()))
        elif model_type == definitions.ModelType.BATTERY:
            pn_max_kw = self._model_configuration['pn_max_kw']
            self._fla_flexibility_model = FLAFlexibilityModel(Battery(*battery_preset(pn_max_kw)),
                                                              model_type=model_type)
        elif model_type == definitions.ModelType.COMBINED_POWER_AND_HEAT:
            p_max_kw = self._model_configuration['p_max_kw']
            self._fla_flexibility_model = FLAFlexibilityModel(CHPLPGSystem(*chp_preset(p_max_kw)),
                                                              model_type=model_type)
        elif model_type == definitions.ModelType.PHOTOVOLTAIC:
            p_kw_peak = self._model_configuration['p_kw_peak']
            self._fla_flexibility_model = FLAFlexibilityModel(PVPlantSystem(*pv_preset(p_peak_kw=p_kw_peak)),
                                                              model_type=model_type)
        """elif model_type == definitions.ModelType.HEAT_VENTILATION_AND_AIR_CONDITIONING:
            model = HVAC(*hvac_preset(pn_max_kw))
            model.inputs.t_air_deg_celsius = 9
            self._fla_flexibility_model = FLAFlexibilityModel(model, model_type=model_type)
        elif model_type == definitions.ModelType.BIOGAS:
            self._fla_flexibility_model = FLAFlexibilityModel(BiogasPlant(*biogas_preset(pn_max_kw)),
                                                              model_type=model_type)"""

    def _handle_flexibility_sample(self, content, meta):
        """
        Method is called if agent role receives a message of the type FlexibilitySample from the controller.
        Now, save the received flexibility sample in dict.
        :param content: message content.
        :param meta: message meta.
        :return: nothing.
        """
        sender_id = meta['sender_id']

        while not self._flexibility_samples or sender_id not in self._flexibility_samples:
            self.context.schedule_instant_task(asyncio.sleep(0.5))

        self._flexibility_samples[sender_id] = content.flexibilities

    def _send_flexibilities_to_other_agents(self, start_date, flex_start, flexibilities, other_agents):
        """
        After receiving a FlexRequest from the controller, send flexibilities to other agents.
        :param start_date:
        :param flex_start: start of the planning horizon for which the sampling is done.
        :param flexibilities: sampled schedules.
        :param other_agents: agents in MAS.
        :return: nothing.
        """
        for other_agent in other_agents:
            if other_agent[1] == self.context.aid:
                continue
            self.context.schedule_instant_task(
                self.context.send_acl_message(
                    content=FlexibilitySample(start_date=start_date, flex_start=flex_start,
                                              flexibilities=flexibilities.to_json()),
                    receiver_addr=other_agent[0],
                    receiver_id=other_agent[1],
                    acl_metadata=self.acl_meta,
                )
            )

    async def flexibility_samples_received(self):
        """
        Check if all flexibility samples have been received.
        :return: if True.
        """
        while not all(entry for entry in self._flexibility_samples.values()):
            await asyncio.sleep(0.5)

    async def feature_information_ack_received(self):
        """
        Check if all feature information acks have been received.
        :return: if True.
        """
        while not self._received_feature_information_ack.done():
            await asyncio.sleep(10)

    def _handle_feature_request(self, content, meta):
        """
        Method in order to handle message of type FeatureRequest from Controller.
        Executes walks in fitness landscapes, calculates features and sends those to Controller.
        :param content:
        :param meta:
        :return:
        """
        print(f'{self.context.aid} handle feature request')
        self.context.schedule_instant_task(self.flexibility_samples_received())
        sampler = qmc.LatinHypercube(d=len(self._flexibility_samples.keys()))
        sample = sampler.integers(definitions.Parameters.NUMBER_OF_SCHEDULES,
                                  n=definitions.Parameters.NUMBER_OF_LANDSCAPES)

        initial_solution = list(self._fla_flexibility_model.generate_flexibilities(number_of_schedules=1)._schedules.
                                values())[0]
        feature_df = pd.DataFrame(columns=['NeighborhoodDefinition', 'TargetSchedule', 'Agent',
                                           'FLA_ID'])

        for combination_i in range(definitions.Parameters.NUMBER_OF_LANDSCAPES):
            combination = [
                list(Flexibilities().from_json(list(self._flexibility_samples.values())[agent_i])._schedules.values())[
                    sampled]
                for agent_i, sampled in enumerate(sample[combination_i])]
            flex_fitness_landscape = FlexFitnessLandscape(agent_id=self.context.aid,
                                                          initial_schedule=initial_solution,
                                                          other_schedules=combination,
                                                          target_schedule=content.target,
                                                          fla_flexibility_model=self._fla_flexibility_model,
                                                          neighborhood_definition=self._neighborhood_definition,
                                                          walk_definition=self._walk_definition)
            flex_fitness_landscape.execute_walk()
            feature_df = flex_fitness_landscape.calculate_features(feature_df=feature_df, features=content.features,
                                                                   col_index=combination_i)
            self._flexibility_fitness_landscapes.append(flex_fitness_landscape)

        self.context.schedule_instant_task(
            self.context.send_acl_message(
                content=FeatureInformation(features=feature_df.to_json()),
                receiver_addr=self._controller_addr,
                receiver_id=self._controller_id,
                acl_metadata=self.acl_meta,
            )
        )
        self._plot_walks(save=True,
                         fig_name=f'Walks_in_FlexFLAs_{self.context.aid}_{definitions.Constants.SCENARIO_UUID}')
        self.context.schedule_instant_task(self.feature_information_ack_received())

    def _plot_walks(self, show=False, save=False, fig_name=''):
        """
        Utility function in order to plot walks.
        :param show: if True, shows plot.
        :param save: if True, saves plot to file.
        :param fig_name: indicates plot name.
        :return: nothing.
        """
        for flex_fla in self._flexibility_fitness_landscapes:
            for walk in flex_fla.walks:
                plt.plot([operating_schedule_candidate.fitness for operating_schedule_candidate in
                          walk.operating_schedule_candidates])
        plt.ylabel('Fitness value')
        plt.title(f'{self.context.aid} - {self._model_configuration["type"]}')
        if show:
            plt.show()
        if save:
            util.directory_exists_or_create(dir_name='walk_figures')
            plt.savefig('walk_figures/' + fig_name)
        plt.clf()


class ControllerRole(Role):
    """
    Role in mango that overtakes tasks of controller.
    """

    def __init__(self, known_agents):
        super().__init__()
        self._known_agents = known_agents
        self._requested_flexibilities = list()
        self._requested_features = list()
        self._received_all_flexibilities = asyncio.Future()
        self._received_all_features = asyncio.Future()
        self._fla_features = [feature.name for feature in LocalFeature]
        self._feature_df = pd.DataFrame()
        self._flexibilities_from_agents = dict()
        self._target_schedule = None

        # generate flexibilities
        self.start_date = definitions.Constants.START_DATE
        self.flex_start = definitions.Constants.FLEX_START

    def setup(self) -> None:
        """
        Set up role -> therefore, subscribe messages.
        :return: nothing.
        """
        self.context.subscribe_message(self, self._handle_flexibility_request_ack,
                                       lambda content, meta: isinstance(content, FlexRequestAck))
        self.context.subscribe_message(self, self._handle_feature_information,
                                       lambda content, meta: isinstance(content, FeatureInformation))
        self.context.schedule_instant_task(self._request_flexibilities())

    async def _request_flexibilities(self):
        """
        ControllerRole is in charge of requesting flexibility information from agents. This is done in this method.
        Afterwards, the controller waits for flexibilities.
        :return: nothing.
        """
        for known_agent in self._known_agents:
            acl_meta = {"sender_addr": self.context.addr, "sender_id": self.context.aid}
            recv_addr = known_agent._agent_context.addr
            recv_aid = known_agent.aid
            other_agents = [(agent._agent_context.addr, agent.aid) for agent in self._known_agents]
            self.context.schedule_instant_task(
                self.context.send_acl_message(
                    content=FlexRequest(start_date=self.start_date, flex_start=self.flex_start,
                                        other_agents=other_agents),
                    receiver_addr=recv_addr,
                    receiver_id=recv_aid,
                    acl_metadata=acl_meta,
                )
            )
            self._requested_flexibilities.append((recv_addr, recv_aid))
        await self._received_all_flexibilities

    def _handle_flexibility_request_ack(self, content, meta):
        """
        Agents send an acknowledgement when receiving flexibility request. This message is handled here.
        :param content: content of the message.
        :param meta: meta information of the message.
        :return: nothing.
        """
        sender_addr = (meta['sender_addr'][0], meta['sender_addr'][1])
        sender_id = meta['sender_id']
        identifier = (sender_addr, sender_id)

        self._flexibilities_from_agents[sender_id] = Flexibilities().from_json(content.flexibilities)

        if identifier in self._requested_flexibilities:
            self._requested_flexibilities.remove(identifier)
        if len(self._requested_flexibilities) == 0:
            self._received_all_flexibilities.set_result(True)
            schedules_from_agents = [flexibility._schedules for flexibility in self._flexibilities_from_agents.values()]
            sample_schedules = [list(schedules.values())[0]._data['p_kw']._values for schedules in
                                schedules_from_agents]
            sum_schedule = sample_schedules[0]
            for schedule_i in range(1, len(sample_schedules)):
                sum_schedule = sum_schedule + sample_schedules[schedule_i]
            self._target_schedule = \
                definitions.Constants.TARGET_SCHEDULE = [(random.normalvariate(timestep_value,
                                                                               timestep_value / 100)) for timestep_value
                                                         in sum_schedule]
            print('Target schedule: ', self._target_schedule)
            self.context.schedule_instant_task(self._request_flex_fla_features())

    async def _request_flex_fla_features(self):
        """
        The controller requests FLA features from agents after flexibility information exchange.
        :return: nothing.
        """
        for known_agent in self._known_agents:
            acl_meta = {"sender_addr": self.context.addr, "sender_id": self.context.aid}
            recv_addr = known_agent._agent_context.addr
            recv_aid = known_agent.aid

            self.context.schedule_instant_task(
                self.context.send_acl_message(
                    content=FeatureRequest(start_date=self.start_date, flex_start=self.flex_start,
                                           target=self._target_schedule, features=self._fla_features),
                    receiver_addr=recv_addr,
                    receiver_id=recv_aid,
                    acl_metadata=acl_meta,
                )
            )
            self._requested_features.append((recv_addr, recv_aid))
        await self._received_all_features

    def _handle_feature_information(self, content, meta):
        """
        This method is called when receiving feature information from the agents.
        :param content: content of the message.
        :param meta: meta information of message.
        :return: nothing.
        """
        sender_addr = (meta['sender_addr'][0], meta['sender_addr'][1])
        sender_id = meta['sender_id']
        acl_meta = {"sender_addr": self.context.addr, "sender_id": self.context.aid}

        identifier = (sender_addr, sender_id)

        self._feature_df = self._feature_df.append(pd.read_json(content.features), ignore_index=True)

        if identifier in self._requested_features:
            self._requested_features.remove(identifier)
        if len(self._requested_features) == 0:
            self._received_all_features.set_result(True)
            new_row = {'Agent': 'Global'}
            for column_name in self._feature_df.columns:
                if column_name in definitions.LocalFeature._member_names_:
                    new_row[f'{column_name}_MEAN'] = self._feature_df[column_name].mean()
                    new_row[f'{column_name}_STD'] = self._feature_df[column_name].std()
            self._feature_df = self._feature_df.append(new_row, ignore_index=True)
            print('Features at Controller are: \n', self._feature_df)
            for column in self._feature_df.columns:
                if 'Unnamed' in column:
                    self._feature_df = self._feature_df.drop(columns=[column])
            if os.path.isfile(f'scenario_data/{definitions.Constants.SCENARIO_UUID}.csv'):
                print('Append dataframe to existing one.')
                df = pd.read_csv(f'scenario_data/{definitions.Constants.SCENARIO_UUID}.csv')
                df = df.append(self._feature_df)
                self._feature_df = df
            util.directory_exists_or_create('scenario_data')
            self._feature_df.to_csv(f'scenario_data/{definitions.Constants.SCENARIO_UUID}.csv')

        self.context.schedule_instant_task(
            self.context.send_acl_message(
                content=FeatureInformationAck(),
                receiver_addr=sender_addr,
                receiver_id=sender_id,
                acl_metadata=acl_meta,
            )
        )
