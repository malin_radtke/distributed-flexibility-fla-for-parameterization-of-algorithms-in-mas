import os

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'

import asyncio
import random
from datetime import datetime
import uuid
from scipy.stats import qmc
from sklearn.metrics import mean_squared_error
import os

import src.definitions as definitions
import src.scenario_presets as scenario_presets
import src.util as util
from src.definitions import NeighborhoodDefinition, WalkDefinition, NeighborhoodDefinitionType
from src.scenario import main

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

random.seed(3)

NUM_AGENTS = 10

SEND_MESSAGES_DELAYED = False

NEIGHBORHOOD_DEFINITION = NeighborhoodDefinition(
    neighborhood_definition_type=NeighborhoodDefinitionType.SINGLE_CHANGE,
    max_distance=10000,
    distance_func=mean_squared_error)

WALK_DEFINITION = WalkDefinition(number_of_steps_per_walk=100,
                                 step_size_factor=0.1)

NUM_SCENARIOS = 10

NUM_TOPOLOGIES = 1

sampler = qmc.LatinHypercube(d=NUM_AGENTS)
model_sample = sampler.integers(len(scenario_presets.model_presets),
                                n=NUM_SCENARIOS)
sampler2 = qmc.LatinHypercube(d=1)
start_sample = sampler2.integers(len(scenario_presets.start_date_presets),
                                 n=NUM_SCENARIOS)

# all low
# all medium
# all medium with distribution
# all high
# half low, half high
# all random different
CHECK_INBOX_INTERVALS = [
    ('small_equal', [0.1]*NUM_AGENTS),
    ('medium_equal', [0.75]*NUM_AGENTS),
    ('high_equal', [1.5]*NUM_AGENTS),
    ('half_small_half_high', [0.1]*int(NUM_AGENTS/2)+[1.5]*int(NUM_AGENTS/2)),
    ('random', [random.random() for _ in range(NUM_AGENTS)])
]


if __name__ == "__main__":
    for scenario_i in range(NUM_SCENARIOS):
        # set date information
        definitions.Constants.START_DATE = \
            scenario_presets.start_date_presets[start_sample[scenario_i][0]]['start_date']
        definitions.Constants.START_DATE_DT = datetime.strptime(definitions.Constants.START_DATE,
                                                                "%Y-%m-%d %H:%M:%S%z")
        definitions.Constants.FLEX_START = \
            scenario_presets.start_date_presets[start_sample[scenario_i][0]]['flex_start']
        # get model configurations
        MODEL_CONFIGURATIONS = [scenario_presets.model_presets[sample_i] for sample_i in model_sample[scenario_i]]

        # get topologies with largest possible difference
        # topology_edge_probabilities = [1/(NUM_TOPOLOGIES-1) * (NUM_TOPOLOGIES-i-1) for i in range(NUM_TOPOLOGIES)]
        topology_edge_probabilities = [0.5]

        # set unique identifier for scenario
        definitions.Constants.SCENARIO_UUID = scenario_id = uuid.uuid4()
        util.plot_model_presets(model_presets=[scenario_presets.model_presets[i] for i in model_sample[scenario_i]],
                                id=scenario_id)
        print(100 * '-')
        print('Start scenario ', definitions.Constants.SCENARIO_UUID)
        print('model configuration: ', model_sample[scenario_i])

        asyncio.run(main(num_agents=NUM_AGENTS,
                         neighborhood_definition=NEIGHBORHOOD_DEFINITION,
                         walk_definition=WALK_DEFINITION,
                         model_configurations=MODEL_CONFIGURATIONS,
                         check_inbox_intervals=CHECK_INBOX_INTERVALS,
                         topology_edge_probabilities=topology_edge_probabilities,
                         send_messages_delayed=SEND_MESSAGES_DELAYED))
