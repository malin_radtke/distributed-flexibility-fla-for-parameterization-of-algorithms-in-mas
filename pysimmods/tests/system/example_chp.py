import os
from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pysimmods.generator.chplpgsystemsim.chplpg_system import CHPLPGSystem
from pysimmods.generator.chplpgsystemsim.presets import chp_preset

AVG_T_AIR = "WeatherCurrent__0___day_avg_t_air_deg_celsius"

WD_PATH = os.path.abspath(
    os.path.join(__file__, "..", "..", "fixtures", "weather-time-series.csv")
)


def main():
    wd = pd.read_csv(WD_PATH, index_col=0)

    chpsys = CHPLPGSystem(*chp_preset(400))
    now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

    steps = 96
    step_size = 900
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    t_storages = np.zeros(steps)
    demands = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        chpsys.set_step_size(step_size)
        chpsys.set_now_dt(now_dt)
        chpsys.inputs.day_avg_t_air_deg_celsius = wd.iloc[widx][AVG_T_AIR]

        chpsys.step()

        p_kws[i] = chpsys.get_p_kw()
        p_th_kws[i] = chpsys.state.p_th_kw
        t_storages[i] = chpsys.state.storage_t_c
        demands[i] = -chpsys.state.e_th_demand_kwh

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, t_storages, demands, "chp_only.png")

    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    t_storages = np.zeros(steps)
    demands = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        chpsys.set_step_size(step_size)
        chpsys.set_now_dt(now_dt)
        chpsys.inputs.day_avg_t_air_deg_celsius = wd.iloc[widx][AVG_T_AIR]
        chpsys.set_p_kw(400)
        chpsys.step()

        p_kws[i] = chpsys.get_p_kw()
        p_th_kws[i] = chpsys.state.p_th_kw
        t_storages[i] = chpsys.state.storage_t_c
        demands[i] = -chpsys.state.e_th_demand_kwh

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, t_storages, demands, "chp_max_power.png")

    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    t_storages = np.zeros(steps)
    demands = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        chpsys.set_step_size(step_size)
        chpsys.set_now_dt(now_dt)
        chpsys.inputs.day_avg_t_air_deg_celsius = wd.iloc[widx][AVG_T_AIR]
        chpsys.set_p_kw(0)
        chpsys.step()

        p_kws[i] = chpsys.get_p_kw()
        p_th_kws[i] = chpsys.state.p_th_kw
        t_storages[i] = chpsys.state.storage_t_c
        demands[i] = -chpsys.state.e_th_demand_kwh

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, t_storages, demands, "chp_min_power.png")

    index = np.arange(steps)
    p_kws = np.zeros(steps)
    p_th_kws = np.zeros(steps)
    t_storages = np.zeros(steps)
    demands = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        chpsys.set_step_size(step_size)
        chpsys.set_now_dt(now_dt)
        chpsys.inputs.day_avg_t_air_deg_celsius = wd.iloc[widx][AVG_T_AIR]
        chpsys.set_p_kw(np.random.randint(0, 400))
        chpsys.step()

        p_kws[i] = chpsys.get_p_kw()
        p_th_kws[i] = chpsys.state.p_th_kw
        t_storages[i] = chpsys.state.storage_t_c
        demands[i] = -chpsys.state.e_th_demand_kwh

        now_dt += timedelta(seconds=step_size)

    plot(index, p_kws, p_th_kws, t_storages, demands, "chp_random_power.png")


def plot(index, p_kws, p_th_kws, t_storages, demands, filename):
    # Plot the results
    _, axes = plt.subplots(3, figsize=(6, 6))

    axes[0].bar(index, p_kws, color="green")
    axes[0].set_ylabel("active power [kW]")

    axes[1].bar(index, p_th_kws, color="orange")
    axes[1].set_ylabel("thermal power [kW]")
    axes[1].bar(index, demands, edgecolor="black", fc=(0, 0, 0, 0))

    axes[2].plot(t_storages, color="red")
    axes[2].set_ylabel("storage temperatore [°C]")
    axes[2].set_xlabel("steps (15-minutes)")
    plt.savefig(filename, dpi=300, bbox_inches="tight")
    plt.close()


def datetime_to_index(
    dt: datetime, step_size: int = 900, steps_per_year: int = 35135
) -> int:
    dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
    dif = dif.total_seconds()
    idx = int(dif // step_size) % steps_per_year

    return idx


if __name__ == "__main__":
    main()
