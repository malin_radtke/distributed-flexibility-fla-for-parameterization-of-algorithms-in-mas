import os
from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem

T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
BH = "WeatherCurrent__0___bh_w_per_m2"
DH = "WeatherCurrent__0___dh_w_per_m2"
WD_PATH = os.path.abspath(
    os.path.join(__file__, "..", "..", "fixtures", "weather-time-series.csv")
)


def pvsys_only():
    wd = pd.read_csv(WD_PATH, index_col=0)

    pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

    now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)

    steps = 96
    step_size = 900
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    q_kvars = np.zeros(steps)
    cos_phis = np.zeros(steps)
    t_modules = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        pvsys.set_step_size(step_size)
        pvsys.set_now_dt(now_dt)
        pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
        pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

        pvsys.step()

        p_kws[i] = pvsys.get_p_kw()
        q_kvars[i] = pvsys.get_q_kvar()
        cos_phis[i] = pvsys.get_cos_phi()
        t_modules[i] = pvsys.state.t_module_deg_celsius

        now_dt += timedelta(seconds=step_size)

    # Plot the results
    plot_results(
        index,
        p_kws,
        q_kvars,
        cos_phis,
        t_modules,
        "pvsys_only.png",
    )


def active_power_request():
    wd = pd.read_csv(WD_PATH, index_col=0)

    pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

    now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)

    steps = 96
    step_size = 900
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    q_kvars = np.zeros(steps)
    cos_phis = np.zeros(steps)
    t_modules = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        pvsys.set_step_size(step_size)
        pvsys.set_now_dt(now_dt)
        pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
        pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

        if 50 < i < 60:
            pvsys.set_p_kw(4)
        pvsys.step()

        p_kws[i] = pvsys.get_p_kw()
        q_kvars[i] = pvsys.get_q_kvar()
        cos_phis[i] = pvsys.get_cos_phi()
        t_modules[i] = pvsys.state.t_module_deg_celsius

        now_dt += timedelta(seconds=step_size)

    # Plot the results
    plot_results(
        index,
        p_kws,
        q_kvars,
        cos_phis,
        t_modules,
        "pvsys_active_power_request.png",
    )


def reactive_power_request():
    wd = pd.read_csv(WD_PATH, index_col=0)

    pvsys = PVPlantSystem(*pv_preset(p_peak_kw=9, cos_phi=0.95))

    now_dt = datetime(2021, 6, 10, 0, 0, 0, tzinfo=timezone.utc)
    steps = 96
    step_size = 900

    index = np.arange(steps)
    p_kws = np.zeros(steps)
    q_kvars = np.zeros(steps)
    cos_phis = np.zeros(steps)
    t_modules = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        pvsys.set_step_size(step_size)
        pvsys.set_now_dt(now_dt)
        pvsys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        pvsys.inputs.bh_w_per_m2 = wd.iloc[widx][BH]
        pvsys.inputs.dh_w_per_m2 = wd.iloc[widx][DH]

        if 25 < i < 35:
            pvsys.set_q_kvar(-0.5)

        if 40 < i < 50:
            pvsys.set_q_kvar(9)
        pvsys.step()

        p_kws[i] = pvsys.get_p_kw()
        q_kvars[i] = pvsys.get_q_kvar()
        cos_phis[i] = pvsys.get_cos_phi()
        t_modules[i] = pvsys.state.t_module_deg_celsius

        now_dt += timedelta(seconds=step_size)

    # Plot the results
    plot_results(
        index,
        p_kws,
        q_kvars,
        cos_phis,
        t_modules,
        "pvsys_reactive_power_request.png",
    )


def datetime_to_index(
    dt: datetime, step_size: int = 900, steps_per_year: int = 35135
) -> int:
    dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
    dif = dif.total_seconds()
    idx = int(dif // step_size) % steps_per_year

    return idx


def plot_results(index, p_kws, q_kvars, cos_phis, t_modules, filename):
    _, axes = plt.subplots(2, 2, figsize=(12, 6))

    axes[0][0].bar(index, p_kws, color="green")
    axes[0][0].set_ylabel("active power [kW]")
    axes[1][0].bar(index, q_kvars, color="blue")
    axes[1][0].set_ylabel("reactive power [kVAr]")

    axes[0][1].plot(cos_phis, color="black")
    axes[0][1].set_ylabel("cos phi")
    axes[1][1].plot(t_modules, color="orange")
    axes[1][1].set_ylabel("module temperature [°C]")

    plt.savefig(filename, dpi=300, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    pvsys_only()
    active_power_request()
    reactive_power_request()
