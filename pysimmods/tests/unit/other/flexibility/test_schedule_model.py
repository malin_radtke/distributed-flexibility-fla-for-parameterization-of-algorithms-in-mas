import unittest
from datetime import datetime

from pysimmods.other.dummy.model import DummyModel
from pysimmods.other.flexibility.schedule import Schedule
from pysimmods.other.flexibility.schedule_model import ScheduleModel
from pysimmods.util.date_util import GER


class TestScheduleModel(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-12-14 13:00:00+0000"
        self.step_size = 900
        model = DummyModel(dict(), dict())
        self.model = ScheduleModel(model)
        self.mabs_mw = ScheduleModel(model, "mw")

    def test_check_inputs(self):
        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model._check_inputs()

        self.assertEqual(900, self.model._step_size)
        self.assertEqual(2021, self.model._now_dt.year)
        self.assertEqual(13, self.model._now_dt.hour)

        self.assertIsNotNone(self.model.schedule)
        self.assertEqual(1, len(self.model.schedule._data))
        self.assertEqual(4, self.model.schedule._data.size)

    def test_setpoints(self):
        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model._check_inputs()

        p_set, q_set = self.model._get_setpoints()

        self.assertEqual(375.0, p_set)
        self.assertEqual(125.0, q_set)

        self.model.set_p_kw(322.2)

        p_set, q_set = self.model._get_setpoints()
        self.assertEqual(322.2, p_set)
        self.assertEqual(125.0, q_set)

        self.model.set_q_kvar(177.3)
        p_set, q_set = self.model._get_setpoints()
        self.assertEqual(322.2, p_set)
        self.assertEqual(177.3, q_set)

        # Schedule hast higher priority
        self.model.schedule.update_row(
            self.start_date, 125.0, 65.0, None, None
        )

        p_set, q_set = self.model._get_setpoints()
        self.assertEqual(125.0, p_set)
        self.assertEqual(65.0, q_set)

    def test_update_schedule(self):
        schedule = Schedule(
            900, 2.0, datetime.strptime(self.start_date, GER), "p_kw", "q_kvar"
        )
        schedule.init()
        schedule._data.p_set_kw = [10.0] * 8

        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model._check_inputs()
        self.model.update_schedule(schedule)

        for target in self.model.schedule._data.p_set_kw:
            self.assertEqual(10, target)

    def test_step(self):
        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model.step()

        self.assertEqual(
            375.0, self.model.schedule.get(self.start_date, "p_set_kw")
        )
        self.assertEqual(
            125.0, self.model.schedule.get(self.start_date, "q_set_kvar")
        )
        self.assertEqual(375, self.model.schedule.get(self.start_date, "p_kw"))
        self.assertEqual(
            125.0, self.model.schedule.get(self.start_date, "q_kvar")
        )

        self.model.set_now_dt("2021-12-14 13:15:00+0000")
        self.model.set_step_size(self.step_size)
        self.model.step()

        # Schedule deletes old entries
        self.assertEqual(1, len(self.model.schedule._data))
        self.assertEqual(4, self.model.schedule._data.size)

    def test_step_q_set(self):
        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model.set_q_kvar(44.3)
        self.model.step()

        self.assertEqual(
            375.0, self.model.schedule.get(self.start_date, "p_set_kw")
        )
        self.assertEqual(
            44.3, self.model.schedule.get(self.start_date, "q_set_kvar")
        )
        self.assertEqual(
            375.0, self.model.schedule.get(self.start_date, "p_kw")
        )
        self.assertEqual(
            44.3, self.model.schedule.get(self.start_date, "q_kvar")
        )

    def test_step_pq_set(self):
        self.model.set_now_dt(self.start_date)
        self.model.set_step_size(self.step_size)
        self.model.set_p_kw(499)
        self.model.set_q_kvar(44.3)
        self.model.step()

        self.assertEqual(
            499.0, self.model.schedule.get(self.start_date, "p_set_kw")
        )
        self.assertEqual(
            44.3, self.model.schedule.get(self.start_date, "q_set_kvar")
        )
        self.assertEqual(
            499.0, self.model.schedule.get(self.start_date, "p_kw")
        )
        self.assertEqual(
            44.3, self.model.schedule.get(self.start_date, "q_kvar")
        )

    def test_step_pq_mw(self):
        self.mabs_mw.set_now_dt(self.start_date)
        self.mabs_mw.set_step_size(self.step_size)
        self.mabs_mw.set_p_kw(499)
        self.mabs_mw.set_q_kvar(44.3)
        self.mabs_mw.step()

        self.assertEqual(
            0.499, self.mabs_mw.schedule.get(self.start_date, "p_set_mw")
        )
        self.assertEqual(
            0.0443, self.mabs_mw.schedule.get(self.start_date, "q_set_mvar")
        )
        self.assertEqual(
            0.499, self.mabs_mw.schedule.get(self.start_date, "p_mw")
        )
        self.assertEqual(
            0.0443, self.mabs_mw.schedule.get(self.start_date, "q_mvar")
        )


if __name__ == "__main__":
    unittest.main()
