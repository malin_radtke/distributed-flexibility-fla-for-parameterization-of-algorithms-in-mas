"""This module contains the tests for the flexibility model."""
import unittest

from pysimmods.other.dummy.model import DummyModel
from pysimmods.other.flexibility.flexibilities import Flexibilities
from pysimmods.other.flexibility.flexibility_model import FlexibilityModel
from pysimmods.other.flexibility.schedule import Schedule


class TestFlexibilityModel(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-01-06 12:00:00+0100"
        self.flex_start = "2021-01-06 12:30:00+0100"
        self.step_size = 900
        self.model = DummyModel(dict(), dict())

    def test_generate_schedules(self):
        """Test the generate schedules functionality."""

        flex = FlexibilityModel(DummyModel(dict(), dict()))

        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.set_p_kw(500)

        flex.step()

        flexis = flex.generate_schedules(self.flex_start, 2, 10)
        self.assertIsInstance(flexis, Flexibilities)
        self.assertEqual(len(flexis), 10)

        for _, val in flexis.items():
            self.assertIsInstance(val, Schedule)
            self.assertEqual(val._data.columns[0], "p_set_kw")
            self.assertEqual(val._data.columns[1], "q_set_kvar")
            self.assertEqual(val._data.columns[2], "p_kw")
            self.assertEqual(val._data.columns[3], "q_kvar")
            for actual in val._data["p_kw"].values:
                self.assertTrue(250 <= actual <= 500)
            for actual in val._data["q_kvar"].values:
                self.assertTrue(-550 <= actual <= 550)
                # Dummy Model p_max_kw = 500

    def test_serializability(self):
        flex = FlexibilityModel(DummyModel(dict(), dict()))

        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.set_p_kw(500)

        flex.step()

        flexis = flex.generate_schedules(self.flex_start, 2, 10)

        json_str = flexis.to_json()

        loaded = Flexibilities().from_json(json_str)

        for key in flexis._schedules:
            diff = flexis[key].compare(loaded[key])
            self.assertTrue(diff.empty)


if __name__ == "__main__":
    unittest.main()
