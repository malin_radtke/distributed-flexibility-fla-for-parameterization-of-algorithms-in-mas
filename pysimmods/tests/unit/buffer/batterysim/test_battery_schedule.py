import unittest
from datetime import datetime, timedelta

from pysimmods.buffer.batterysim.battery import Battery
from pysimmods.buffer.batterysim.presets import battery_preset
from pysimmods.other.flexibility.schedule import Schedule
from pysimmods.other.flexibility.schedule_model import ScheduleModel
from pysimmods.util.date_util import GER


class TestBatterySchedule(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-12-14 00:00:00+0000"
        self.now_dt = datetime.strptime(self.start_date, GER)
        self.step_size = 900

    def test_schedule_operation(self):
        model = Battery(*battery_preset(5))
        smodel = ScheduleModel(Battery(*battery_preset(5)))

        schedule = Schedule(self.step_size, 1, self.now_dt)
        schedule.init()
        now_dt = self.now_dt
        for _ in range(80):
            schedule.update_entry(now_dt, "p_set_kw", -0.1)
            now_dt += timedelta(seconds=self.step_size)

        smodel.set_now_dt(self.now_dt)
        smodel.set_step_size(900)
        smodel.update_schedule(schedule)

        now_dt = self.now_dt
        for idx in range(96):
            model.set_now_dt(now_dt)
            model.set_step_size(900)
            smodel.set_now_dt(now_dt)
            smodel.set_step_size(900)
            model.step()
            smodel.step()

            now_dt += timedelta(seconds=self.step_size)
            s_p_kw = smodel.get_p_kw()
            if idx < 80:
                # Schedule active
                if s_p_kw != 0:
                    self.assertNotEqual(s_p_kw, model.get_p_kw())
                self.assertLessEqual(s_p_kw, 0)
            elif idx < 90:
                # Schedule not active anymore
                self.assertEqual(s_p_kw, model.get_p_kw())
            else:
                # Battery is empty in normal operation
                pass


if __name__ == "__main__":
    unittest.main()
