import unittest

from pysimmods.generator.biogassim import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset


class TestBiogas(unittest.TestCase):
    def setUp(self):
        self.start_date = "2017-01-01 00:00:00+0000"
        self.params, self.inits = biogas_preset(pn_max_kw=555)
        self.params["sign_convention"] = "passive"

    def test_step(self):
        """Test the normal step function."""
        biogas = BiogasPlant(self.params, self.inits)

        biogas.set_p_kw(-555)
        biogas.set_step_size(15 * 60)
        biogas.set_now_dt(self.start_date)

        biogas.step()

        self.assertEqual(-555, biogas.get_p_kw())
        self.assertAlmostEqual(694.659, biogas.state.p_th_kw, places=3)
        self.assertEqual(62.5, biogas.state.gas_prod_m3)
        self.assertAlmostEqual(79.81546, biogas.state.gas_cons_m3, places=3)
        self.assertAlmostEqual(49.307, biogas.state.gas_fill_percent, places=3)

    def test_default_schedule(self):
        biogas = BiogasPlant(self.params, self.inits)

        biogas.set_step_size(15 * 60)
        biogas.set_now_dt(self.start_date)

        biogas.step()

        self.assertEqual(-277.5, biogas.get_p_kw())
        self.assertAlmostEqual(364.442, biogas.state.p_th_kw, places=3)

    def test_set_p_kw(self):
        biogas = BiogasPlant(self.params, self.inits)

        biogas.set_now_dt(self.start_date)
        biogas.set_step_size(15 * 60)
        biogas.set_p_kw(555)

        biogas.step()

        self.assertEqual(-555, biogas.get_p_kw())
        self.assertEqual(0, biogas.state.q_kvar)

    def test_sign_convention(self):
        params, inits = biogas_preset(pn_max_kw=555)
        params["sign_convention"] = "active"
        biogas = BiogasPlant(params, inits)

        biogas.set_p_kw(555)
        biogas.set_step_size(15 * 60)
        biogas.set_now_dt(self.start_date)

        biogas.step()

        self.assertEqual(555, biogas.get_p_kw())
        self.assertAlmostEqual(694.659, biogas.state.p_th_kw, places=3)
        self.assertEqual(62.5, biogas.state.gas_prod_m3)
        self.assertAlmostEqual(79.81546, biogas.state.gas_cons_m3, places=3)
        self.assertAlmostEqual(49.307, biogas.state.gas_fill_percent, places=3)

    def test_get_state(self):
        biogas = BiogasPlant(self.params, self.inits)

        biogas.set_now_dt(self.start_date)
        biogas.set_step_size(15 * 60)
        biogas.set_p_kw(555)

        biogas.step()

        state = biogas.get_state()
        base_state = state["state"]
        chp0_state = state["chp-0"]
        chp1_state = state["chp-1"]
        chp2_state = state["chp-2"]

        for key, value in base_state.items():
            self.assertEqual(value, getattr(biogas.state, key))
        for key, value in chp0_state.items():
            self.assertEqual(value, getattr(biogas.chps[0].state, key))
        for key, value in chp1_state.items():
            self.assertEqual(value, getattr(biogas.chps[1].state, key))
        for key, value in chp2_state.items():
            self.assertEqual(value, getattr(biogas.chps[2].state, key))

    def test_set_state(self):
        biogas = BiogasPlant(self.params, self.inits)
        state = biogas.get_state()

        pkws1 = list()
        for _ in range(5):
            biogas.set_now_dt(self.start_date)
            biogas.set_step_size(15 * 60)
            biogas.set_p_kw(555)
            biogas.step()
            pkws1.append(biogas.get_p_kw())

        biogas.set_state(state)

        pkws2 = list()
        for _ in range(5):
            biogas.set_now_dt(self.start_date)
            biogas.set_step_size(15 * 60)
            biogas.set_p_kw(555)
            biogas.step()
            pkws2.append(biogas.get_p_kw())

        for idx in range(5):
            self.assertEqual(pkws1[idx], pkws2[idx])


if __name__ == "__main__":
    unittest.main()
