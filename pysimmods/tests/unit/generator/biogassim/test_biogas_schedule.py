import unittest
from datetime import datetime, timedelta

from pysimmods.generator.biogassim import BiogasPlant
from pysimmods.generator.biogassim.presets import biogas_preset
from pysimmods.other.flexibility.schedule import Schedule
from pysimmods.other.flexibility.schedule_model import ScheduleModel
from pysimmods.util.date_util import GER


class TestScheduleModel(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-12-14 00:00:00+0000"
        self.now_dt = datetime.strptime(self.start_date, GER)
        self.step_size = 900
        self.params, self.inits = biogas_preset(pn_max_kw=555)
        self.params["sign_convention"] = "passive"

    def test_schedule_operation(self):
        model = BiogasPlant(self.params, self.inits)
        smodel = ScheduleModel(
            BiogasPlant(self.params, self.inits),
            step_size=900,
            now_dt=self.now_dt,
        )

        schedule = Schedule(
            self.step_size,
            1,
            self.now_dt,
        )
        schedule.init()
        now_dt = self.now_dt
        for _ in range(80):
            schedule.update_entry(now_dt, "p_set_kw", 138.75)
            now_dt += timedelta(seconds=self.step_size)

        smodel.update_schedule(schedule)
        now_dt = self.now_dt
        for idx in range(96):
            model.set_now_dt(now_dt)
            model.set_step_size(900)
            smodel.set_now_dt(now_dt)
            smodel.set_step_size(900)
            model.step()
            smodel.step()

            now_dt += timedelta(seconds=self.step_size)
            s_p_kw = smodel.get_p_kw()

            if s_p_kw != 0 and idx < 95:
                self.assertNotEqual(s_p_kw, model.get_p_kw())
            self.assertLessEqual(s_p_kw, 0)


if __name__ == "__main__":
    unittest.main()
