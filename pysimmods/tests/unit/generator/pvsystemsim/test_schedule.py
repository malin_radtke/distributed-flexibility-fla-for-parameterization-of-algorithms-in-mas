"""This module contains the tests for the :class:`.ForecastModel`."""
import os
import unittest
from datetime import datetime, timedelta

import pandas as pd
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem
from pysimmods.other.flexibility.schedule_model import ScheduleModel
from pysimmods.util.date_util import GER

T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
BH = "WeatherCurrent__0___bh_w_per_m2"
DH = "WeatherCurrent__0___dh_w_per_m2"


class TestScheduleModel(unittest.TestCase):
    def setUp(self):
        self.params, self.inits = pv_preset(9)
        self.start_date = "2006-08-01 12:00:00+0100"

        ts_path = os.path.abspath(
            os.path.join(
                __file__,
                "..",
                "..",
                "..",
                "..",
                "fixtures",
                "weather-time-series.csv",
            )
        )
        self.ts = pd.read_csv(ts_path, index_col=0)

    def test_normal_operation(self):
        pvsys = PVPlantSystem(*pv_preset(9))
        smod = ScheduleModel(
            PVPlantSystem(self.params, self.inits),
            unit="kw",
        )

        p_kws = []
        q_kvars = []
        for mod in [pvsys, smod]:
            mod.set_step_size(3600)
            mod.set_now_dt(self.start_date)
            mod.set_p_kw(6)
            mod.inputs.t_air_deg_celsius = 10
            mod.inputs.bh_w_per_m2 = 100
            mod.inputs.dh_w_per_m2 = 100
            mod.step()

            p_kws.append(mod.get_p_kw())
            q_kvars.append(mod.get_q_kvar())

        self.assertEqual(*p_kws)
        self.assertEqual(*q_kvars)

    def test_schedule_operation(self):
        pvsys = PVPlantSystem(*pv_preset(9))
        smod = ScheduleModel(PVPlantSystem(self.params, self.inits))
        now_dt = datetime.strptime(self.start_date, GER)

        smod.set_now_dt(now_dt)
        smod.set_step_size(3600)

        schedule = pd.DataFrame(
            {"p_set_kw": [0.5407789, 0.1, 0.1, 0.1, 0.01, 0.001]},
            index=pd.date_range(start=now_dt, freq="1h", periods=6),
        )
        smod.update_schedule(schedule)
        p_kws = {0: [], 1: []}
        q_kvars = {0: [], 1: []}
        steps = 6
        io = 9
        for step in range(steps):
            for idx, mod in enumerate([pvsys, smod]):
                widx = (step + io) * 4
                mod.set_step_size(3600)
                mod.set_now_dt(now_dt)
                mod.inputs.t_air_deg_celsius = self.ts.iloc[widx][T_AIR]
                mod.inputs.bh_w_per_m2 = self.ts.iloc[widx][BH]
                mod.inputs.dh_w_per_m2 = self.ts.iloc[widx][DH]

                mod.step()
                p_kws[idx].append(mod.get_p_kw())
                q_kvars[idx].append(mod.get_q_kvar())

            now_dt += timedelta(hours=1)

        self.assertAlmostEqual(p_kws[0][0], p_kws[1][0])

        for p1, p2 in zip(p_kws[0][1:], p_kws[1][1:]):
            self.assertNotEqual(p1, p2)


if __name__ == "__main__":
    unittest.main()
