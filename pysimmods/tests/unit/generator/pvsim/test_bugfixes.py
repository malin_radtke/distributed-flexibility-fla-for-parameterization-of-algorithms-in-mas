import os
import unittest
from datetime import datetime, timedelta

# import matplotlib
# import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pysimmods.generator.pvsim.pvp import PhotovoltaicPowerPlant
from pysimmods.util import irradiance, solargeometry
from pysimmods.util.date_util import GER

# matplotlib.use("TkAgg")
T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
BH = "WeatherCurrent__0___bh_w_per_m2"
DH = "WeatherCurrent__0___dh_w_per_m2"


class TestPV(unittest.TestCase):
    def setUp(self):
        self.params = {"a_m2": 4.12}
        self.inits = {"t_module_deg_celsius": 5}

        ts_path = os.path.abspath(
            os.path.join(
                __file__,
                "..",
                "..",
                "..",
                "..",
                "fixtures",
                "weather-time-series.csv",
            )
        )
        self.ts = pd.read_csv(ts_path, index_col=0)
        self.start_date = datetime.strptime("2011-01-01 00:00:00+0100", GER)

    def test_module_temperature_rising(self):
        pv_plant = PhotovoltaicPowerPlant(self.params, self.inits)

        step_size = 1 * 3600
        idx_incr = step_size / 3_600
        steps = int(1200 / idx_incr)

        t_module = np.zeros(1200)
        now_dt = self.start_date

        for step in range(steps):
            pv_plant.set_step_size(step_size)
            pv_plant.set_now_dt(now_dt)
            pv_plant.inputs.t_air_deg_celsius = self.ts.iloc[step][T_AIR]
            pv_plant.inputs.bh_w_per_m2 = self.ts.iloc[step][BH]
            pv_plant.inputs.dh_w_per_m2 = self.ts.iloc[step][DH]

            pv_plant.step()

            t_module[step] = pv_plant.state.t_module_deg_celsius
            now_dt += timedelta(seconds=step_size)

        self.assertLess(t_module.max(), 75)

    def test_calculate_module_temperature(self):
        pv_plant = PhotovoltaicPowerPlant(self.params, self.inits)

        step_size = 1 * 3600
        idx_incr = step_size / 3_600
        steps = int(1200 / idx_incr)
        # print(f"StepSize={step_size}, incr={idx_incr:.2f} steps={steps}")

        irradiances = np.zeros(steps)
        azimuths = np.zeros(steps)
        elevations = np.zeros(steps)
        now_dt = self.start_date

        for step in range(steps):
            sun_pos = solargeometry.compute_sun_position(
                now_dt.timestamp(),
                pv_plant.config.lat_deg,
                pv_plant.config.lon_deg,
            )
            azimuths[step] = sun_pos["azimuth"]
            elevations[step] = sun_pos["elevation"]

            irradiances[step] = irradiance.tilted_surface(
                self.ts.iloc[int(step * idx_incr)][BH],
                self.ts.iloc[int(step * idx_incr)][DH],
                pv_plant.config.tilt_deg,
                pv_plant.config.orient_deg,
                sun_pos["elevation"],
                sun_pos["azimuth"],
            )
            now_dt += timedelta(seconds=step_size)

        pv_plant.inputs.step_size = step_size
        t_changes = np.zeros(steps)
        t_modules = np.zeros(steps)

        for step in range(steps):
            pv_plant.inputs.t_air_deg_celsius = self.ts.iloc[
                int(step * idx_incr)
            ][T_AIR]

            t_changes[step] = pv_plant._calculate_module_temperature(
                irradiances[step]
            )

            pv_plant.state.t_module_deg_celsius += t_changes[step]
            t_modules[step] = pv_plant.state.t_module_deg_celsius

        # fig, axes = plt.subplots(3, 2)
        # axes[0][0].plot(azimuths)
        # axes[1][0].plot(elevations)
        # axes[2][0].plot(irradiances)
        # axes[0][1].plot(t_modules)
        # axes[1][1].plot(t_changes)
        # plt.show()
        # print(f"T_module\nMax: {t_modules.max()}\nMin: {t_modules.min()}")
        # print(f"T_change\nMax: {t_changes.max()}\nMin: {t_changes.min()}")
        self.assertLess(t_modules.max(), 75)
        self.assertLess(t_changes.max(), 40)


if __name__ == "__main__":
    unittest.main()
