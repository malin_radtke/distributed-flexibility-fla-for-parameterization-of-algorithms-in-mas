"""Tests for the CHP system, which consists of a CHP and
a household heat demand model.

TODO: add tests for edge cases.

"""
import unittest

from pysimmods.generator.chplpgsystemsim import CHPLPGSystem
from pysimmods.generator.chplpgsystemsim.presets import chp_preset


class TestCHP(unittest.TestCase):
    def setUp(self):
        self.params, self.inits = chp_preset(7)
        self.params["sign_convention"] = "passive"

    def test_step(self):
        chpsys = CHPLPGSystem(self.params, self.inits)

        chpsys.set_now_dt("2017-01-01 00:00:00+0000")
        chpsys.set_p_kw(-7)
        chpsys.set_step_size(900)
        chpsys.inputs.day_avg_t_air_deg_celsius = 6.0

        chpsys.step()

        self.assertEqual(chpsys.get_p_kw(), -7)
        self.assertEqual(chpsys.state.p_th_kw, 17.99)

    def test_default_schedule(self):
        chpsys = CHPLPGSystem(self.params, self.inits)

        chpsys.set_now_dt("2017-01-01 00:00:00+0000")
        chpsys.set_step_size(900)
        chpsys.inputs.day_avg_t_air_deg_celsius = 6.0

        chpsys.step()

        self.assertEqual(chpsys.config.default_schedule[0], 50)
        self.assertEqual(chpsys.get_p_kw(), -3.5)
        self.assertEqual(chpsys.state.p_th_kw, 8.995)

    def test_set_percent(self):
        chpsys = CHPLPGSystem(self.params, self.inits)

        chpsys.set_now_dt("2017-01-01 00:00:00+0000")
        chpsys.set_percent(100)
        chpsys.set_step_size(900)
        chpsys.inputs.day_avg_t_air_deg_celsius = 6.0

        chpsys.step()

        self.assertEqual(chpsys.get_p_kw(), -7)
        self.assertEqual(chpsys.state.p_th_kw, 17.99)

    def test_sign_convention(self):
        self.params["sign_convention"] = "active"
        chpsys = CHPLPGSystem(self.params, self.inits)

        chpsys.set_now_dt("2017-01-01 00:00:00+0000")
        chpsys.set_p_kw(7)
        chpsys.set_step_size(900)
        chpsys.inputs.day_avg_t_air_deg_celsius = 6.0

        chpsys.step()

        self.assertEqual(chpsys.get_p_kw(), 7)
        self.assertEqual(chpsys.state.p_th_kw, 17.99)


if __name__ == "__main__":
    unittest.main()
