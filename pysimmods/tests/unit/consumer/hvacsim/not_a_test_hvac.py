import os
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pysimmods.consumer.hvacsim.presets import hvac_preset
from pysimmods.consumer.hvacsim import HVAC
from pysimmods.other.flexibility.flexibility_model import (
    FlexibilityModel,
)
from pysimmods.util.date_util import GER


class TestHVACExtras:
    def __init__(self):
        self.power = 60
        self.params, self.inits = hvac_preset(self.power)
        self.start_date = "2019-08-11 04:00:00+0100"
        # self.t_airs = [15.6 + (idx / 100) for idx in range(4 * 24 * 366)]
        self.t_airs = [16.0] * (4 * 24 * 366)

    def test_default_schedule(self):
        hvac = HVAC(self.params, self.inits)
        flex = FlexibilityModel(hvac, self.start_date, 900)

        steps = np.arange(4 * 24 * 365)

        forecasts = pd.DataFrame(
            data=np.array([self.t_airs]).reshape(-1, 1),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=4 * 24 * 366,
                freq="15T",
            ),
        )
        flex.update_forecasts(forecasts)
        p_kws = np.zeros_like(steps, dtype=np.float32)
        t_cs = np.zeros_like(steps, dtype=np.float32)

        for idx in steps:
            print(f"Processing step {idx} ...", end="\r")
            flex.inputs.t_air_deg_celsius = self.t_airs[idx]
            flex.step()
            p_kws[idx] = flex.state.p_kw
            t_cs[idx] = flex.state.theta_t_deg_celsius
        print()
        print("Done stepping")

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

        ax1.plot(steps, p_kws, label="power [kW]")
        ax1.legend()
        ax2.plot(steps, t_cs, label="temperature [°C]")
        ax2.legend()
        plt.show()

    def test_energy_behavior(self):
        # hvac = HVAC(self.params, self.inits)
        days = 365
        t_air = 16.0
        step_size = 900

        # for _ in range(96):
        #     hvac.inputs.t_air_deg_celsius = t_air
        #     hvac.inputs.step_size = step_size
        #     hvac.step()
        #     # print(f"{hvac.state.p_kw} kW, {hvac.state.theta_t_deg_celsius} °C")

        hvac = HVAC(self.params, self.inits)

        steps = np.arange(4 * 24 * days)
        p_kws = np.zeros_like(steps, dtype=np.float32)
        t_cs = np.zeros_like(steps, dtype=np.float32)
        e_kwh = 0
        t_c = 0

        for idx in steps:
            hvac.inputs.t_air_deg_celsius = t_air
            hvac.inputs.step_size = step_size
            hvac.step()
            p_kws[idx] = hvac.state.p_kw
            t_cs[idx] = hvac.state.theta_t_deg_celsius
            e_kwh += hvac.state.p_kw
            t_c += hvac.state.theta_t_deg_celsius

        workload = e_kwh / (len(steps) * self.power) * 100
        print(f"{workload:.2f} % workload")
        e_kwh *= step_size / 3_600
        print(f"{e_kwh:.1f} kWh/year")

        e_per_d = e_kwh / days
        print(f"{e_per_d:.3f} kWh/day")

        a_m = (
            self.params["width"]
            * self.params["height"]
            * self.params["length"]
        )
        print(f"{a_m} m³ volume")

        e_per_m_a = e_kwh / a_m
        print(f"{e_per_m_a:.3f} kWh/m³*year")

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

        ax1.plot(steps, p_kws, label="power [kW]")
        ax1.legend()
        ax2.plot(steps, t_cs, label="temperature [°C]")
        ax2.legend()
        plt.show()


def main():
    the = TestHVACExtras()
    the.test_energy_behavior()
    # the.test_default_schedule()


if __name__ == "__main__":
    main()
