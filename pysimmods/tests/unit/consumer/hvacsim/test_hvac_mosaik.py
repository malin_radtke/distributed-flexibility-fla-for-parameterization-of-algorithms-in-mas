import unittest

from pysimmods.consumer.hvacsim import HVAC
from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator


class TestSimulator(unittest.TestCase):
    def setUp(self):
        self.sim = PysimmodsSimulator()

        self.sim.init(
            sid="TestSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )

        self.inputs = {
            "HVAC-0": {
                "t_air_deg_celsius": {
                    "DummyWeather-0.WeatherDummy-0": 6.0,
                },
                "p_set_mw": {
                    "DummyCtrl-0.CtrlDummy-0": 0.001,
                },
            },
            "HVAC-1": {
                "t_air_deg_celsius": {
                    "DummyWeather-0.WeatherDummy-0": 6.0,
                },
            },
        }

        self.hvac_params = {
            "p_max_kw": 2.0,
            "eta_percent": 200.0,
            "l_m": 4.0,
            "w_m": 5.0,
            "h_m": 2.5,
            "d_m": 0.25,
            "lambda_w_per_m_k": 0.5,
            "t_min_deg_celsius": 17.0,
            "t_max_deg_celsius": 23.0,
        }
        self.hvac_inits = {
            "mass_kg": 500.0,
            "c_j_per_kg_k": 2390.0,
            "theta_t_deg_celsius": 21.0,
            "cooling": True,
            "mode": "auto",
        }

    def test_create_hvac(self):
        entities = self.sim.create(
            num=3,
            model="HVAC",
            params=self.hvac_params,
            inits=self.hvac_inits,
        )

        self.assertEqual(len(entities), 3)
        for entity in entities:
            self.assertIsInstance(entity, dict)
            self.assertIsInstance(self.sim.models[entity["eid"]], HVAC)

    def test_step(self):
        self.sim.create(
            num=2,
            model="HVAC",
            params=self.hvac_params,
            inits=self.hvac_inits,
        )

        self.sim.step(0, self.inputs)

        # Internally, the models use load reference system in kw
        self.assertEqual(self.sim.models["HVAC-0"].get_p_kw(), 1.0)
        self.assertEqual(self.sim.models["HVAC-1"].get_p_kw(), 2.0)

    def test_get_data(self):
        self.sim.create(
            num=2,
            model="HVAC",
            params=self.hvac_params,
            inits=self.hvac_inits,
        )

        self.sim.step(0, self.inputs)
        outputs = {"HVAC-0": ["p_mw"], "HVAC-1": ["p_mw"]}

        data = self.sim.get_data(outputs)
        num_vals = 0

        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 2)

        # From outside, the generator reference system in mw is used
        self.assertEqual(data["HVAC-0"]["p_mw"], 0.001)
        self.assertEqual(data["HVAC-1"]["p_mw"], 0.002)

    def test_sign(self):
        self.hvac_params["sign_convention"] = "active"
        self.sim.create(
            num=2,
            model="HVAC",
            params=self.hvac_params,
            inits=self.hvac_inits,
        )

        self.sim.step(0, self.inputs)
        outputs = {"HVAC-0": ["p_mw"], "HVAC-1": ["p_mw"]}

        data = self.sim.get_data(outputs)
        num_vals = 0

        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 2)

        # From outside, the generator reference system in mw is used
        self.assertEqual(data["HVAC-0"]["p_mw"], -0.001)
        self.assertEqual(data["HVAC-1"]["p_mw"], -0.002)


if __name__ == "__main__":
    unittest.main()
