import unittest

from pysimmods.other.dummy.qgenerator import DummyQGenerator

from pysimmods.model.qgenerator import QControl


class TestQGenerator(unittest.TestCase):
    def test_get_q_min_kvar(self):
        """Test if q_min_kvar is calculated correctly."""
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.PRIORITIZE_P,
            },
            {},
        )

        self.assertEqual(0, gen.get_qn_min_kvar())

    def test_get_q_max_kvar(self):
        """Test if q_min_kvar is calculated correctly."""
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.PRIORITIZE_P,
            },
            {},
        )

        self.assertEqual(600, gen.get_qn_max_kvar())

    def test_set_q_kvar_psc(self):
        """Test the correct behavior of the set_q_kvar function."""

        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.PRIORITIZE_P,
            },
            {},
        )
        gen.set_q_kvar(None)  # No set point
        self.assertEqual(None, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(0, gen.get_q_kvar())

        gen.set_q_kvar(0)  # Turn off
        self.assertEqual(0, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(0, gen.get_q_kvar())

        gen.set_q_kvar(-600)  # Full generation
        self.assertEqual(600, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(-600, gen.get_q_kvar())

        gen.set_q_kvar(600)  # Full consumption
        self.assertEqual(-600, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(600, gen.get_q_kvar())

        gen.set_q_kvar(-700)  # Generation too high
        self.assertEqual(700, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(-600, gen.get_q_kvar())

    def test_set_q_kvar_asc(self):
        """Test the correct behavior of the set_q_kvar function."""
        gen = DummyQGenerator(
            {
                "sign_convention": "active",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.PRIORITIZE_P,
            },
            dict(),
        )
        gen.set_q_kvar(None)  # No set point
        self.assertEqual(None, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(0, gen.get_q_kvar())

        gen.set_q_kvar(0)  # Turn off
        self.assertEqual(0, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(0, gen.get_q_kvar())

        gen.set_q_kvar(600)  # Full generation
        self.assertEqual(600, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(600, gen.get_q_kvar())

        gen.set_q_kvar(-600)  # Full consumption
        self.assertEqual(-600, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(-600, gen.get_q_kvar())

        gen.set_q_kvar(700)  # Generation too high
        self.assertEqual(700, gen.inputs.q_set_kvar)
        gen.step()
        self.assertEqual(600, gen.get_q_kvar())

    @unittest.skip
    def test_get_percent_in_p_dominated(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.P_SET,
            },
            dict(),
        )

        # Input not set
        self.assertIsNone(gen.get_percent_in())

        gen.set_p_kw(0)  # Turn off
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(-500)  # Full generation
        self.assertEqual(100, gen.get_percent_in())

        gen.set_p_kw(-375)  # Half generation
        self.assertEqual(50, gen.get_percent_in())

        # Minimum generation, needs to be larger than zero
        gen.set_p_kw(-250)
        self.assertEqual(0.01, gen.get_percent_in())

        gen.set_p_kw(-125)  # Not feasible. Turn off
        self.assertEqual(0, gen.get_percent_in())

        gen.set_p_kw(125)  # Not feasible, can't consume energy
        self.assertEqual(0, gen.get_percent_in())

    @unittest.skip
    def test_get_percent_in_q_dominated(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.Q_SET,
            },
            dict(),
        )

        gen.set_q_kvar(0)
        self.assertEqual(0, gen.get_percent_in())

        gen.set_q_kvar(-600)
        self.assertEqual(100, gen.get_percent_in())

        gen.set_q_kvar(-300)
        self.assertEqual(50, gen.get_percent_in())

        gen.set_q_kvar(75)
        self.assertEqual(0, gen.get_percent_in())

    @unittest.skip
    def test_get_percent_in_cos_phi_dominated(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.COS_PHI_SET,
            },
            dict(),
        )

        gen.set_cos_phi(0)
        self.assertEqual(0, gen.get_percent_in())

        gen.set_cos_phi(1.0)
        self.assertEqual(100, gen.get_percent_in())

        gen.set_cos_phi(-0.2)
        self.assertEqual(0, gen.get_percent_in())

        gen.set_cos_phi(1.2)
        self.assertEqual(100, gen.get_percent_in())

    @unittest.skip
    def test_set_percent_p_set(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "cos_phi": 0.9,
                "q_control": QControl.P_SET,
            },
            dict(),
        )

        gen.set_percent(0)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(100)
        self.assertEqual(500, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(-10)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(110)
        self.assertEqual(500, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)
        self.assertEqual(0.9, gen.inputs.cos_phi)

    @unittest.skip
    def test_set_percent_q_set(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "cos_phi": 0.9,
                "q_control": QControl.Q_SET,
            },
            dict(),
        )

        gen.set_percent(0)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(100)
        self.assertEqual(600, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(-10)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertEqual(0.9, gen.inputs.cos_phi)

        gen.set_percent(110)
        self.assertEqual(600, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertEqual(0.9, gen.inputs.cos_phi)

    @unittest.skip
    def test_set_percent_cos_phi_set(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.COS_PHI_SET,
            },
            dict(),
        )

        gen.set_percent(0)
        self.assertEqual(0, gen.inputs.cos_phi)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)

        gen.set_percent(100)
        self.assertEqual(1, gen.inputs.cos_phi)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)

        gen.set_percent(-10)
        self.assertEqual(0, gen.inputs.cos_phi)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)

        gen.set_percent(110)
        self.assertEqual(1, gen.inputs.cos_phi)
        self.assertIsNone(gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.q_set_kvar)

    @unittest.skip
    def test_set_percent_pq_set(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.PQ_SET,
            },
            dict(),
        )

        gen.set_percent(0)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(0.01)
        self.assertEqual(250, gen.inputs.p_set_kw)
        self.assertAlmostEqual(545.436, gen.inputs.q_set_kvar, places=3)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(100)
        self.assertEqual(500, gen.inputs.p_set_kw)
        self.assertAlmostEqual(331.662, gen.inputs.q_set_kvar, places=3)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(-10)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(110)
        self.assertEqual(500, gen.inputs.p_set_kw)
        self.assertAlmostEqual(331.662, gen.inputs.q_set_kvar, places=3)
        self.assertIsNone(gen.inputs.cos_phi)

    @unittest.skip
    def test_set_percent_qp_set(self):
        gen = DummyQGenerator(
            {
                "sign_convention": "passive",
                "p_max_kw": 500,
                "p_min_kw": 250,
                "s_max_kva": 600,
                "q_control": QControl.QP_SET,
            },
            dict(),
        )

        gen.set_percent(0)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(0.01)
        self.assertAlmostEqual(0.06, gen.inputs.q_set_kvar, places=3)
        self.assertEqual(500, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(100)
        self.assertEqual(600, gen.inputs.q_set_kvar)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(-10)
        self.assertEqual(0, gen.inputs.q_set_kvar)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.cos_phi)

        gen.set_percent(110)
        self.assertEqual(600, gen.inputs.q_set_kvar)
        self.assertEqual(0, gen.inputs.p_set_kw)
        self.assertIsNone(gen.inputs.cos_phi)


if __name__ == "__main__":
    unittest.main()
