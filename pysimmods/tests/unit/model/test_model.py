"""This module contains the unit test for the base model class."""
import unittest
from unittest.mock import MagicMock

from pysimmods.other.dummy.model import DummyModel


class TestModel(unittest.TestCase):
    """Test class for the base model class."""

    def test_get_state(self):
        """Test for the *get_state* method."""
        model = DummyModel(dict(), dict())
        model.state = MagicMock()
        model.state.attr1 = "foo"
        model.state.attr2 = 1.234

        state = model.get_state()
        self.assertIn("attr1", state)
        self.assertEqual(state["attr1"], "foo")

        self.assertIn("attr2", state)
        self.assertEqual(state["attr2"], 1.234)

    def test_set_state(self):
        """Test for the *set_state* method."""
        model1 = DummyModel(dict(), dict())
        model1.state = MagicMock()
        model1.state.attr1 = "foo"
        model1.state.attr2 = 1.234

        state = model1.get_state()

        model2 = DummyModel(dict(), dict())
        model2.state = MagicMock()
        model2.set_state(state)

        self.assertTrue(hasattr(model2.state, "attr1"))
        self.assertEqual(model2.state.attr1, "foo")

        self.assertTrue(hasattr(model2.state, "attr2"))
        self.assertEqual(model2.state.attr2, 1.234)


if __name__ == "__main__":
    unittest.main()
