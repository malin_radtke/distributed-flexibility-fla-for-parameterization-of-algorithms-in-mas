from pysimmods.model.state import ModelState


class DieselGenState(ModelState):
    """State of the Diesel generator."""
