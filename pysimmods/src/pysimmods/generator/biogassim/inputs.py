"""This module contains the input model for the Biogas plant."""
from pysimmods.model.inputs import ModelInputs


class BiogasInputs(ModelInputs):
    """Input parameters for the biogas plant model.

    The biogas model currently requires only general inputs, see
    :class:`.ModelInputs`.

    """
