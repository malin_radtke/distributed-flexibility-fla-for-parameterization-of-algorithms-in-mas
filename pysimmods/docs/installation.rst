Installation
============

Pysimmods requires Python >= 3.8 and is available on https://pypi.org. 
It can be installed, preferably into a virtualenv, with 

.. code-block:: bash

    pip install pysimmods

Alternatively, you can clone this repository with

.. code-block:: bash

    git clone https://gitlab.com/midas-mosaik/pysimmods.git

then switch into the pysimmods folder and type

.. code-block:: bash
    
    pip install -e .
