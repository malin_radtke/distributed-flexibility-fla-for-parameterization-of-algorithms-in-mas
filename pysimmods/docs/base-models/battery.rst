Battery
=======

Self-discharge and aging are not considered. 
Effect of charging power on efficiency eta is modelled by fitting a polynomial model to data measured by TU Munich.

Instantiation
-------------

You have to provide two dictionaries ``params`` and ``inits``.
The dict ``params`` provides the parameters for the battery model and should contain the following content:

.. code-block:: python

    params = {
        "cap_kwh": 5,
        "p_charge_max_kw": 1,
        "p_discharge_max_kw": 1,
    }

The parameter ``cap_kwh`` specifies the capacity of the battery in kilo watt hours.
The parameters ``p_charge_max_kw`` and ``p_discharge_max_kw`` determine the maximum charging and discharging power in kilo watt.
The battery also has two optional parameters.
With ``soc_min_percent`` a threshold for the state of charge can be defined.
The state of charge may not fall below that value.
The parameter ``eta_pc`` are the coefficients of the polynom to model the efficiency of the battery, represented as list of three values.

The dict ``inits`` provides initial values for the state of the battery.

.. code-block:: python

    inits = {"soc_percent": 50}

This defines the state of charge of the battery.

Parameters of the Battery 
-------------------------

tbd

Simulating a Day
----------------

The battery has no special input requirements for simulation.
It has a default schedule that aims to keep the state of charge constant from one day to the other.
This means there is no power input required.

.. code-block:: python
    
    battery = Battery(*battery_preset(5))
    now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

    # Simulate for one day
    for i in range(steps):
        battery.set_step_size(step_size)
        battery.set_now_dt(now_dt)
        battery.step()

        power[i] = battery.get_p_kw()
        charge[i] = battery.state.soc_percent

        now_dt += timedelta(seconds=step_size)

Plotting the results, we get something like:

.. image:: battery_only.png
    :width: 800

Providing Setpoints
-------------------

Of course, individual setpoints for the battery can be provided.
E.g., we can explore how long it takes to full discharge the battery.
We will use a different resolution of one minute (``step_size = 60``).

.. code-block:: python

    index = []  # We don't know how long it takes; no numpy this time
    power = []
    charge = []

    battery.state.soc_percent = 100
    minute = 0

    while battery.state.soc_percent > battery.config.soc_min_percent:
        minute += 1
        battery.set_step_size(step_size)
        battery.set_now_dt(now_dt)
        battery.set_p_kw(-1)
        battery.step()

        index.append(minute)
        power.append(battery.get_p_kw())
        charge.append(battery.state.soc_percent)

        now_dt += timedelta(seconds=step_size)
    
The result looks like:

.. image:: battery_discharge.png
    :width: 800

We see that in step 83 the battery does not discharge with full power because the minimal state of charge is reached.
This corresponds to 20 hours and ~37.5 minutes.
Note that this only applies if the ratio between `cap_kwh` and `p_discharge_max_kw` is 5:1.

Next, we will charge the battery and see how long that takes. 
The code is mostly the same, just a flipped sign at the setpoint.

The result looks like:

.. image:: battery_charge.png
    :width: 800

This process takes 88 steps or 22 hours.
Note that this only applies if the ratio between `cap_kwh` and `p_charge_max_kw` is 5:1.


Full Code for the Example
-------------------------

.. code-block:: python

    from datetime import datetime, timedelta, timezone

    import matplotlib.pyplot as plt
    import numpy as np
    from pysimmods.buffer.batterysim.battery import Battery
    from pysimmods.buffer.batterysim.presets import battery_preset


    def main():

        # First, create an instance of a battery model
        battery = Battery(*battery_preset(5))

        now_dt = datetime(2021, 6, 8, 0, 0, 0, tzinfo=timezone.utc)

        step_size = 900
        steps = 96
        index = np.arange(steps)
        power = np.zeros(steps)
        charge = np.zeros(steps)

        # Simulate for one day
        for i in range(steps):
            battery.set_step_size(step_size)
            battery.set_now_dt(now_dt)
            battery.step()

            power[i] = battery.get_p_kw()
            charge[i] = battery.state.soc_percent

            now_dt += timedelta(seconds=step_size)

        # Plot the results
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(index, power, color="green")
        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(charge, color="red")
        axes[1].set_xlabel("steps (15-minutes)")
        axes[1].set_ylabel("state of charge [%]")
        plt.savefig("battery_only.png", dpi=300, bbox_inches="tight")
        plt.close()

        # How long does it take to discharge the battery?
        index = []  # We don't know how long it takes; no numpy this time
        power = []
        charge = []

        battery.state.soc_percent = 100
        steps = 0

        while battery.state.soc_percent > battery.config.soc_min_percent:
            battery.set_step_size(step_size)
            battery.set_now_dt(now_dt)
            battery.set_p_kw(-1)
            battery.step()

            index.append(steps)
            power.append(battery.get_p_kw())
            charge.append(battery.state.soc_percent)

            steps += 1
            now_dt += timedelta(seconds=step_size)

        print(f"Number of steps: {steps}. Total duration: {steps*900/60} minutes.")
        # Plot the results
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(index, power, color="green")
        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(charge, color="red")
        axes[1].set_xlabel("steps (15-minutes)")
        axes[1].set_ylabel("state of charge [%]")
        plt.savefig("battery_discharge.png", dpi=300, bbox_inches="tight")
        plt.close()

        # How long does it take to charge the battery?
        index = []
        power = []
        charge = []

        steps = 0

        while battery.state.soc_percent < 100:
            battery.set_step_size(step_size)
            battery.set_now_dt(now_dt)
            battery.set_p_kw(1)
            battery.step()

            index.append(steps)
            power.append(battery.get_p_kw())
            charge.append(battery.state.soc_percent)

            steps += 1
            now_dt += timedelta(seconds=step_size)

        print(f"Number of steps: {steps}. Total duration: {steps*900/60} minutes.")
        # Plot the results
        _, axes = plt.subplots(2, figsize=(6, 6))

        axes[0].bar(index, power, color="green")
        axes[0].set_ylabel("active power [kW]")

        axes[1].plot(charge, color="red")
        axes[1].set_xlabel("steps (15-minutes)")
        axes[1].set_ylabel("state of charge [%]")
        plt.savefig("battery_charge.png", dpi=300, bbox_inches="tight")
        plt.close()


    if __name__ == "__main__":
        main()
